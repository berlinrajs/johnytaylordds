//
//  ActionImageView.swift
//  AceDental
//
//  Created by SRS Web Solutions on 28/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ActionImageView: UIImageView {

    private var labelTitle: UILabel!
    override func drawRect(rect: CGRect, forViewPrintFormatter formatter: UIViewPrintFormatter) {
        
    }
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        layer.borderColor = borderColor.CGColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
    }
    override func willMoveToSuperview(newSuperview: UIView?) {
        super.willMoveToSuperview(newSuperview)
        layer.borderColor = borderColor.CGColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
    }
    
    var borderColor: UIColor = UIColor.whiteColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }
    
    var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    var title: String = "" {
        willSet {
            if labelTitle == nil {
                labelTitle = UILabel(frame: CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame)))
                labelTitle.font = UIFont.systemFontOfSize(15)
                labelTitle.textColor = UIColor.whiteColor()
                labelTitle.textAlignment = NSTextAlignment.Center
                
                self.addSubview(labelTitle)
            }
        }
        didSet {
            self.labelTitle.text = title
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if labelTitle != nil {
            labelTitle.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetWidth(self.frame))
        }
    }
    
    func addTarget(target: AnyObject, selector: Selector) {
        self.userInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer(target: target, action: selector)
        tapGesture.numberOfTapsRequired = 1
        self.addGestureRecognizer(tapGesture)
    }

}
