//
//  FinancialPolicyPopUp.swift
//  EyePhysiciansOfLongBench
//
//  Created by SRS Web Solutions on 25/07/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ToothRemovalPopUp: UIView {
    
//    static let sharedInstance = NSBundle.mainBundle().loadNibNamed("ToothRemovalPopUp", owner: nil, options: nil).first as! ToothRemovalPopUp
    
    class func popUpView() -> ToothRemovalPopUp {
        return NSBundle.mainBundle().loadNibNamed("ToothRemoval", owner: nil, options: nil)!.first as! ToothRemovalPopUp
    }
    

    @IBOutlet weak var textFieldToothNo: PDTextField!
    @IBOutlet weak var textFieldBP1: PDTextField!
    @IBOutlet weak var textFieldBP2: PDTextField!
    @IBOutlet weak var textFieldPulse: PDTextField!
    
    var viewController: UIViewController!
    var completion:((String, String)->Void)?
    
    func show(inViewController: UIViewController, completion : (toothNo : String, pulse : String) -> Void) {
        
        textFieldToothNo.text = ""
//        textFieldBP1.text = ""
//        textFieldBP2.text = ""
        textFieldPulse.text = ""
       
        self.completion = completion
        
        viewController = inViewController
        viewController.view.addSubview(self)
        
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonOkAction() {
        if textFieldToothNo.isEmpty {
            let alert = Extention.alert("PLEASE ENTER THE TOOTH NO")
            self.viewController.presentViewController(alert, animated: true, completion: nil)
        } else {
            self.completion?(textFieldToothNo.text!,
            textFieldPulse.text!)
            self.close()
        }
    }
    
    @IBAction func buttonCancelAction() {
        self.close()
        self.completion?("", "")
    }
    
    func close() {
        //        self.subviews[0].transform = CGAffineTransformIdentity
        //        UIView.animateWithDuration(0.2, animations: {
        //            self.subviews[0].transform = CGAffineTransformMakeScale(0.2, 0.2)
        //            }) { (finished) in
        self.removeFromSuperview()
        //        }
    }
}
extension ToothRemovalPopUp: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldToothNo {
            return textField.formatToothNumbers(range, string: string)
        } else {
            return textField.formatNumbers(range, string: string, count: 3)
        }
    }
}
