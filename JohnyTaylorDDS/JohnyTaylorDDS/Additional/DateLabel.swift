//
//  DateLabel.swift
//   Angell Family Dentistry
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DateLabel: PDLabel {

    var todayDate: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.whiteColor()
    }
    override func willMoveToSuperview(newSuperview: UIView?) {
        super.willMoveToSuperview(newSuperview)
        
        text = "Tap to Date"
        textColor = UIColor.lightGrayColor()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(DateLabel.labelDateTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        self.addGestureRecognizer(tapGesture)
    }
    
    func labelDateTapped(sender: AnyObject?) {
        text = todayDate
        textColor = UIColor.blackColor()
    }
    var dateTapped: Bool {
        get {
            return text != "Tap to Date"
        }
    }
    
    var setDate: Bool = false {
        didSet {
            if setDate == true {
                labelDateTapped(nil)
            }
        }
    }
    
    
}
