//
//  PDTextField.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 18/02/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDTextField: UITextField {

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        layer.borderColor = enabled ? borderColor.CGColor : borderColor.colorWithAlphaComponent(0.2).CGColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: enabled ? placeHolderColor : placeHolderColor.colorWithAlphaComponent(0.2)])
        tintColor = enabled ? placeHolderColor : placeHolderColor.colorWithAlphaComponent(0.2)
        clipsToBounds = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let leftView = UIView(frame: CGRectMake(0, 0, 10, frame.height))
        self.leftView = leftView
        leftViewMode = .Always
    }
    
    var borderColor: UIColor = UIColor.whiteColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }
    
    var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }

    var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    var placeHolderColor: UIColor = UIColor.whiteColor().colorWithAlphaComponent(0.5) {
        didSet {
            attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: placeHolderColor])
            tintColor = placeHolderColor
        }
    }
    
    
//    //MARK: Initializers
//    override func prepareForInterfaceBuilder() {
//        super.prepareForInterfaceBuilder()
//        layer.borderColor = borderColor.CGColor
//        layer.borderWidth = borderWidth
//        layer.cornerRadius = cornerRadius
//    }
//
//    func setup() {
//        layer.borderColor = UIColor.whiteColor().CGColor
//        layer.borderWidth = 1.0
//        layer.cornerRadius = 3.0
//    }
//    
//    func configure() {
//        layer.borderColor = borderColor.CGColor
//        layer.borderWidth = borderWidth
//        layer.cornerRadius = cornerRadius
//    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        layer.borderColor = borderColor.CGColor
//        layer.borderWidth = borderWidth
//        layer.cornerRadius = cornerRadius
//        clipsToBounds = true
//    }
}
