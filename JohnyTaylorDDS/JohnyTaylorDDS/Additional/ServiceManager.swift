//
//  ServiceManager.swift
//   Angell Family Dentistry
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016  Angell Family Dentistry. All rights reserved.
//

import UIKit

let hostUrl = "https://johnytaylor.srswebsolutions.com/johnytaylor/eaglesoft/"



class ServiceManager: NSObject {
    
    class func fetchDataFromService(baseUrlString: String, serviceName: String, parameters : [String : String]?, success:(result : AnyObject) -> Void, failure :(error : NSError) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: NSURL(string: baseUrlString))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.POST(serviceName, parameters: parameters, progress: { (progress) in
            print(progress.fractionCompleted)
            }, success: { (task, result) in
                //                print(task)
                //                print(result)
                success(result: result!)
        }) { (task, error) in
            //            print(task)
            //            print(error)
            failure(error: error)
        }
    }
    class func postReview(name: String, comment: String, rating: CGFloat, phoneNumber: String, allowMessage: Bool, email: String, anonymous: Bool, completion: (success: Bool, error: NSError?) -> Void) {
        //https://alpha.mncell.com/review/app_review.php
        ServiceManager.fetchDataFromService("https://alpha.mncell.com/review/", serviceName: "app_review.php", parameters: ["patient_appkey": "mcJohnnyTaylor", "patient_name": name, "patient_review_comment": comment, "patient_rating": "\(rating)", "patient_phone_number": phoneNumber, "sms_allowed": allowMessage == true ? "1" : "0", "patient_email": email, "user_anonymous": anonymous == true ? "1" : "0"], success: { (result) in
            if result["posts"] != nil && (result["posts"]!!)["status"] as! String == "success" {
                completion(success: true, error: nil)
            } else {
                completion(success: false, error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]!!)["message"] as! String]))
            }
        }) { (error) in
            completion(success: false, error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong, Please Check your internet connection and try again"]))
        }
    }
    class func loginWithUsername(userName: String, password: String, completion: (success: Bool, error: NSError?) -> Void) {
        ServiceManager.fetchDataFromService("http://mncell.com/mclogin/", serviceName: "apploginapi.php?", parameters: ["appkey": "mcjt", "username": userName, "password": password], success: { (result) in
            if (result["posts"]!)!["status"] as! String == "success" {
                completion(success: true, error: nil)
            } else {
                completion(success: false, error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]!)!["message"] as! String]))
            }
        }) { (error) in
            completion(success: false, error: nil)
        }
    }
    
    #if AUTO
    class func sendPatientDetails(patient: PDPatient, completion:(success : Bool, error: NSError?) -> Void) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        let date = dateFormatter.dateFromString(patient.dateOfBirth)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var params: [String: String] = [String: String]()
        params["Birthdate"] = dateFormatter.stringFromDate(date!)
        params["FName"] = patient.firstName
        params["LName"] = patient.lastName
        if let _ = patient.city {
            params["City"] = patient.city
            params["Email"] = patient.email
            params["Address"] = patient.addressLine
            params["Zip"] = patient.zipCode
            params["HmPhone"] = patient.phoneNumber.numbers
            params["work_phone"] = patient.employerPhone?.numbers
            params["cellular_phone"] = patient.cellPhoneNumber?.numbers
            params["sex"] = patient.gender.rawValue
            if patient.maritialStatus.index != 3 {
                params["marital_status"] = patient.maritialStatus.rawValue
            }
            params["State"] = patient.state
        }
        
        if let ssn = patient.socialSecurityNumber {
            params["SSN"] = ssn.numbers
        }
        if let patientDetails = patient.patientDetails {
            params["PatNum"] = patientDetails.patientNumber.isEmpty ? "0" : patientDetails.patientNumber
        } else {
            params["PatNum"] = "0"
        }
        let manager = AFHTTPSessionManager(baseURL: NSURL(string: hostUrl))
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.POST("consent_form_add_newpatient_op.php", parameters: params, progress: { (progress) in
            
            }, success: { (task, result) in
                if let patientId = result?["patient_id"] as? String {
                    if let patientDetails = patient.patientDetails {
                        patientDetails.patientNumber = patientId
                    } else {
                        patient.patientDetails = PatientDetails()
                        patient.patientDetails?.patientNumber = patientId
                    }
                }
                completion(success: true, error: nil)
        }) { (task, error) in
            completion(success: false, error: error)
        }
    }
    #endif
}
