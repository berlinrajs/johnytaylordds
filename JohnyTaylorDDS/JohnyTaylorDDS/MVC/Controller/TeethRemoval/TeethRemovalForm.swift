//
//  TeethRemovalForm.swift
//  JohnyTaylorDDS
//
//  Created by SRS Web Solutions on 16/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class TeethRemovalForm: PDViewController {
    @IBOutlet var imageViewPatientSignature: UIImageView!
    @IBOutlet var imageViewDoctorSignature: UIImageView!
    @IBOutlet var imageViewWitnessSignature: UIImageView!
    @IBOutlet var labelDate1: FormLabel!
    @IBOutlet var labelDate2: FormLabel!
    @IBOutlet var labelDate3: FormLabel!
    @IBOutlet var labelDate4: FormLabel!
    @IBOutlet var labelDate5: FormLabel!
    @IBOutlet var labelBP: FormLabel!
    @IBOutlet var labelPulse: FormLabel!
    @IBOutlet var labeltoothNo: FormLabel!
    @IBOutlet var labelOthers1: FormLabel!
    @IBOutlet var labelOthers2: FormLabel!
    @IBOutlet var labelOthers3: FormLabel!
    @IBOutlet var imageViewIntials: UIImageView!
    @IBOutlet var labelPatientName: FormLabel!
    @IBOutlet var labelPatientName2: FormLabel!
    @IBOutlet var labelPatientName3: FormLabel!
    @IBOutlet var labelWitnessName: FormLabel!
    @IBOutlet var labelDoctorName: FormLabel!
    
    @IBOutlet var multiSelectButton: [UIButton]!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday
        labelDate4.text = patient.dateToday
        labelDate5.text = patient.dateToday
        imageViewPatientSignature.image = patient.patientSignature
        imageViewDoctorSignature.image = patient.doctorSign
        imageViewWitnessSignature.image = patient.witnessSignature
        imageViewIntials.image = patient.initialsImage
        labelPatientName.text = patient.fullName
        labelPatientName2.text = patient.fullName
        labelPatientName3.text = patient.fullName
        labelWitnessName.text = patient.witnessName
//        if patient.toothRemoval.removalBp1.isEmpty && patient.toothRemoval.removalBp2.isEmpty {
//            labelBP.text = ""
//        }
//        else {
//        labelBP.text = "\(patient.toothRemoval.removalBp1) over \(patient.toothRemoval.removalBp2)"
//        }
//        labelPulse.text = patient.toothRemoval.removalPulse
        labelOthers1.text = patient.textOthers1
//        labelOthers2.text = patient.textOthers2
        labelOthers3.text = patient.textOthers3
//        labeltoothNo.text = patient.toothRemoval.removalToothNo
        labelDoctorName.text = "Dr. Johnny D. Taylor, DDS"
        for btn in multiSelectButton{
            btn.selected = patient.arrayToothRemoval.containsObject(btn.tag)
            
        }
        for form in patient.selectedForms {
            if form.formTitle == kToothRemoval {
                labeltoothNo.text = form.toothNumbers
                break
            }
        }
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
