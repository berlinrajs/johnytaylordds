//
//  TeethRemovalStep1VC.swift
//  JohnyTaylorDDS
//
//  Created by Berlin Raj on 11/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class TeethRemovalStep1VC: PDViewController {
    @IBOutlet var textViewComments1: PDTextView!
     @IBOutlet var textViewComments2: PDTextView!
     @IBOutlet var textViewComments3: PDTextView!
     @IBOutlet var imageViewInitials: SignatureView!
    @IBOutlet var imageViewPatientSignature: SignatureView!
    @IBOutlet var imageViewDoctorSignature: UIImageView!
    @IBOutlet var imageViewWitnessSignature: SignatureView!
    @IBOutlet var textFieldWitnessName: PDTextField!
    @IBOutlet var labelDate1: DateLabel!
    @IBOutlet var labelDate2: DateLabel!
    @IBOutlet var labelDate3: DateLabel!
    var multiSelectArray : NSMutableArray!
    
    @IBOutlet var arrayButton: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday
        imageViewDoctorSignature.image = patient.doctorSign
    
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        self.textViewDidBeginEditing(textViewComments1)
        textViewComments1.text = patient.textOthers1
        self.textViewDidEndEditing(textViewComments1)
        
        self.textViewDidBeginEditing(textViewComments3)
        textViewComments3.text = patient.textOthers3
        self.textViewDidEndEditing(textViewComments3)
        textFieldWitnessName.text = patient.witnessName
        
        self.multiSelectArray = patient.arrayToothRemoval == nil ? NSMutableArray() : patient.arrayToothRemoval.mutableCopy() as! NSMutableArray
        for button in arrayButton {
            button.selected = self.multiSelectArray.containsObject(button.tag)
        }
    }
    
    func saveValues() {
        patient.patientSignature = imageViewPatientSignature.image
        patient.arrayToothRemoval = self.multiSelectArray
        patient.witnessSignature = imageViewWitnessSignature.image
        patient.initialsImage = imageViewInitials.image
        self.textViewDidBeginEditing(textViewComments1)
        patient.textOthers1 = textViewComments1.text
        self.textViewDidEndEditing(textViewComments1)
        
        self.textViewDidBeginEditing(textViewComments3)
        patient.textOthers3 = textViewComments3.text
        self.textViewDidEndEditing(textViewComments3)
        patient.witnessName = textFieldWitnessName.text
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }
    
    @IBAction func multiSelectAction(sender: UIButton) {
        sender.selected = !sender.selected
        if multiSelectArray.containsObject(sender.tag)
        {
            multiSelectArray.removeObject(sender.tag)
        }
        else {
            multiSelectArray.addObject(sender.tag)
        }
    }
    
    
    
    @IBAction func buttonActionDone(sender: AnyObject) {
        if multiSelectArray.count == 0{
            let alert = Extention.alert("PLEASE SELECT ANY ONE OF THE ABOVE PROCEDURES")
            self.presentViewController(alert, animated: true, completion: nil)
        
        } else if textViewComments1.isEmpty ||  textViewComments3.isEmpty || textViewComments1.text == "TYPE HERE" || textViewComments3.text == "TYPE HERE" {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if textFieldWitnessName.isEmpty{
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !imageViewPatientSignature.isSigned() ||  !imageViewWitnessSignature.isSigned() || !imageViewInitials.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            saveValues()
            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("TeethRemovalForm") as! TeethRemovalForm
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension TeethRemovalStep1VC : UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "TYPE HERE" || textView.text == "" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
