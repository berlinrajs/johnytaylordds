//
//  CompositeFillingForm.swift
//  JohnyTaylorDDS
//
//  Created by SRS Web Solutions on 12/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CompositeFillingForm: PDViewController {
    
    @IBOutlet var imageViewPatientSignature: UIImageView!
    @IBOutlet var imageViewDoctorSignature: UIImageView!
    @IBOutlet var imageViewWitnessSignature: UIImageView!
    @IBOutlet var imageViewPatientInitial: UIImageView!
    @IBOutlet var labelDate1: FormLabel!
    @IBOutlet var labelDate2: FormLabel!
    @IBOutlet var labelDate3: FormLabel!
     @IBOutlet var labelOthers: FormLabel!
    @IBOutlet var labelPatientName: FormLabel!
    @IBOutlet var buttonCollection1: [UIButton]!
    @IBOutlet var buttonCollection2: [UIButton]!
 

    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewPatientSignature.image = patient.patientSignature
        imageViewDoctorSignature.image = patient.doctorSign
        imageViewWitnessSignature.image = patient.witnessSignature
        imageViewPatientInitial.image = patient.initialsImage
        labelPatientName.text = patient.fullName
        labelOthers.text = patient.popUpText
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday
        for btn in buttonCollection1{
            btn.selected = patient.arrayCompositeFilling1.containsObject(btn.tag)
        }
        for btn in buttonCollection2{
            btn.selected = patient.arrayCompositeFilling2.containsObject(btn.tag)
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
