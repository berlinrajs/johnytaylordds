//
//  CompositeFillingsVc.swift
//  JohnyTaylorDDS
//
//  Created by SRS Web Solutions on 12/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CompositeFillingsVc: PDViewController {
    
    @IBOutlet var imageViewPatientSignature: SignatureView!
    @IBOutlet var imageViewDoctorSignature: UIImageView!
    @IBOutlet var imageViewWitnessSignature: SignatureView!
    @IBOutlet var imageViewPatientInitial: SignatureView!
    @IBOutlet var labelDate1: DateLabel!
    @IBOutlet var labelDate2: DateLabel!
    @IBOutlet var labelDate3: DateLabel!
    @IBOutlet var buttonMain: UIButton!
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    var arrayCompositeFilling1 : NSMutableArray!
    var arrayCompositeFilling2 : NSMutableArray!
    
    @IBOutlet var arrayButton1: [UIButton]!
    @IBOutlet var arrayButton2: [UIButton]!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday
        imageViewDoctorSignature.image = patient.doctorSign
        button1.userInteractionEnabled = false
        button1.alpha = 0.5
        button2.userInteractionEnabled = false
        button2.alpha = 0.5
        button3.userInteractionEnabled = false
        button3.alpha = 0.5
        
        loadValues()
    }
    
    func loadValues() {
        self.arrayCompositeFilling1 = patient.arrayCompositeFilling1 == nil ? NSMutableArray() : patient.arrayCompositeFilling1.mutableCopy() as! NSMutableArray
        self.arrayCompositeFilling2 = patient.arrayCompositeFilling2 == nil ? NSMutableArray() : patient.arrayCompositeFilling2.mutableCopy() as! NSMutableArray
        
        for button in arrayButton1 {
            button.selected = arrayCompositeFilling1.containsObject(button.tag)
            
            if button.tag == 4  && button.selected == true {
                button1.userInteractionEnabled = true
                button1.alpha = 1
                button2.userInteractionEnabled = true
                button2.alpha = 1
                button3.userInteractionEnabled = true
                button3.alpha = 1
                
                button1.selected = arrayCompositeFilling1.containsObject(5)
                button2.selected = arrayCompositeFilling1.containsObject(6)
                button3.selected = arrayCompositeFilling1.containsObject(7)
                
            } else if button.tag == 4  && button.selected == false {
                
                button1.userInteractionEnabled = false
                button1.selected = false
                button1.alpha = 0.5
                button2.userInteractionEnabled = false
                button2.selected = false
                button2.alpha = 0.5
                button3.userInteractionEnabled = false
                button3.alpha = 0.5
                button3.selected = false
                arrayCompositeFilling1.removeObject(5)
                arrayCompositeFilling1.removeObject(6)
                arrayCompositeFilling1.removeObject(7)
            }
        }
        
        for button in arrayButton2 {
            button.selected = arrayCompositeFilling2.containsObject(button.tag)
        }
    }
    
    func saveValues() {
        patient.arrayCompositeFilling1 = self.arrayCompositeFilling1
        patient.arrayCompositeFilling2 = self.arrayCompositeFilling2
        patient.initialsImage = imageViewPatientInitial.signatureImage()
        patient.patientSignature = imageViewPatientSignature.signatureImage()
        patient.witnessSignature = imageViewWitnessSignature.signatureImage()
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }

    @IBAction func buttonArray1(sender: UIButton) {
        sender.selected = !sender.selected
        if arrayCompositeFilling1.containsObject(sender.tag) {
            arrayCompositeFilling1.removeObject(sender.tag)
        } else {
            arrayCompositeFilling1.addObject(sender.tag)
        }
        if sender.tag == 4  && sender.selected == true {
            button1.userInteractionEnabled = true
            button1.alpha = 1
            button2.userInteractionEnabled = true
            button2.alpha = 1
            button3.userInteractionEnabled = true
            button3.alpha = 1
            
        } else if sender.tag == 4  && sender.selected == false {
          
            button1.userInteractionEnabled = false
            button1.selected = false
            button1.alpha = 0.5
            button2.userInteractionEnabled = false
            button2.selected = false
            button2.alpha = 0.5
            button3.userInteractionEnabled = false
            button3.alpha = 0.5
            button3.selected = false
            arrayCompositeFilling1.removeObject(5)
            arrayCompositeFilling1.removeObject(6)
            arrayCompositeFilling1.removeObject(7)
        }
    }
 
    @IBAction func buttonArray2(sender: UIButton) {
        sender.selected = !sender.selected
        if arrayCompositeFilling2.containsObject(sender.tag) {
            arrayCompositeFilling2.removeObject(sender.tag)
        } else {
            arrayCompositeFilling2.addObject(sender.tag)
        }
        if sender.tag == 8  && sender.selected == true
        {
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE TYPE HERE") { (textView, isEdited) in
                if isEdited {
                    self.patient.popUpText = textView.text
                } else {
                    sender.selected = false
                    self.arrayCompositeFilling2.removeObject(sender.tag)
                    self.patient.popUpText = ""
                }
            }
        }
        else{
            
            self.patient.popUpText = ""
        }
    }

    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if arrayCompositeFilling1.count == 0{
            let alert = Extention.alert("PLEASE SELECT ANY ONE OF THE ABOVE PROCEDURES")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if (buttonMain.selected == true && (!button1.selected && !button2.selected && !button3.selected)) {
                let alert = Extention.alert("PLEASE SELECT ANY ONE OF THE ABOVE PROCEDURES")
                self.presentViewController(alert, animated: true, completion: nil)
        } else if arrayCompositeFilling2.count == 0{
            let alert = Extention.alert("PLEASE SELECT ANY ONE OF THE ABOVE PROCEDURES")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !imageViewPatientSignature.isSigned() ||  !imageViewWitnessSignature.isSigned() || !imageViewPatientInitial.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            saveValues()
            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("CompositeFillingForm") as! CompositeFillingForm
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        
    }

   
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
