//
//  HipaaConsentVc.swift
//  JohnyTaylorDDS
//
//  Created by SRS Web Solutions on 31/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class HipaaConsentVc: PDViewController {
    
    
    @IBOutlet var imageViewInitial1: SignatureView!
    @IBOutlet var imageViewInitial2: SignatureView!
    @IBOutlet var imageViewInitial3: SignatureView!
    @IBOutlet var imageViewInitial4: SignatureView!
    
    @IBOutlet var textFieldName1: PDTextField!
    @IBOutlet var textFieldName2: PDTextField!
    @IBOutlet var textFieldName3: PDTextField!
    
    @IBOutlet var textFieldPhone1: PDTextField!
    @IBOutlet var textFieldPhone2: PDTextField!
    @IBOutlet var textFieldPhone3: PDTextField!
    
    @IBOutlet var textFieldRelationship1: PDTextField!
    @IBOutlet var textFieldRelationship2: PDTextField!
    @IBOutlet var textFieldRelationship3: PDTextField!
    
    
    @IBOutlet var imageViewSignaturePatient: SignatureView!
    @IBOutlet var imageViewParent: SignatureView!
    
    @IBOutlet var labelDate: DateLabel!
    @IBOutlet var labelDate2: DateLabel!
    
    @IBOutlet var buttonPhone: UIButton!
     @IBOutlet var buttonEmail: UIButton!
     @IBOutlet var buttonPhone2: UIButton!
    
    var arrayHipaa : NSMutableArray!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday

        // Do any additional setup after loading the view.
        loadValues()
    }
    
    func loadValues() {
        self.arrayHipaa = patient.arrayHipaa == nil ? NSMutableArray() : patient.arrayHipaa.mutableCopy() as! NSMutableArray
        
        buttonPhone.selected = self.arrayHipaa.containsObject(buttonPhone.tag)
        buttonEmail.selected = self.arrayHipaa.containsObject(buttonEmail.tag)
        buttonPhone2.selected = self.arrayHipaa.containsObject(buttonPhone2.tag)
        
        textFieldName1.text = patient.hipaaName1
        textFieldName2.text = patient.hipaaName2
        textFieldName3.text = patient.hipaaName3
        textFieldPhone1.text = patient.hipaaphone1
        textFieldPhone2.text = patient.hipaaPhone2
        textFieldPhone3.text = patient.hipaaPhone3
        textFieldRelationship1.text = patient.hipaaRelationShip1
        textFieldRelationship2.text = patient.hipaaRelationShip2
        textFieldRelationship3.text = patient.hipaaRelationShip3
    }
    
    func saveValues() {
        patient.arrayHipaa = self.arrayHipaa
        patient.hipaaInitial1 = imageViewInitial1.image
        patient.hipaaInitial2 = imageViewInitial2.image
        patient.hipaaInitial3 = imageViewInitial3.image
        patient.hipaaInitial4 = imageViewInitial4.image
        patient.hipaaName1 = textFieldName1.text
        patient.hipaaName2 = textFieldName2.text
        patient.hipaaName3 = textFieldName3.text
        patient.hipaaphone1 = textFieldPhone1.text
        patient.hipaaPhone2 = textFieldPhone2.text
        patient.hipaaPhone3 = textFieldPhone3.text
        patient.hipaaRelationShip1 = textFieldRelationship1.text
        patient.hipaaRelationShip2 = textFieldRelationship2.text
        patient.hipaaRelationShip3 = textFieldRelationship3.text
        patient.signature1 = imageViewSignaturePatient.image
        patient.signature2 = imageViewParent.image
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }

    @IBAction func multiSelectAction(sender: UIButton) {
        sender.selected = !sender.selected
        if arrayHipaa.containsObject(sender.tag) {
            arrayHipaa.removeObject(sender.tag)
        } else {
            arrayHipaa.addObject(sender.tag)
        }
        
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if  (buttonEmail.selected || buttonPhone.selected || buttonPhone2.selected) &&  !imageViewInitial1.isSigned()  {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if stage1notfilled() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if stage2notfilled() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if stage3notfilled() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if  !textFieldPhone1.isEmpty && !textFieldPhone1.text!.isPhoneNumber || !textFieldPhone2.isEmpty && !textFieldPhone2.text!.isPhoneNumber || !textFieldPhone3.isEmpty && !textFieldPhone3.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE")
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if !imageViewSignaturePatient.isSigned() ||  !imageViewParent.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped || !labelDate2.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            saveValues()
            
            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("HipaaConsentForm") as! HipaaConsentForm
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        
    }
    
    
    
    func stage1notfilled() -> Bool{
        if textFieldName1.isEmpty && textFieldPhone1.isEmpty && textFieldRelationship1.isEmpty
        {
            return false
        } else if textFieldName1.isEmpty || textFieldPhone1.isEmpty || textFieldRelationship1.isEmpty {
            return true
        }
        else {
            return false
            
        }
    }
    
    func stage2notfilled() -> Bool{
        if textFieldName2.isEmpty && textFieldPhone2.isEmpty && textFieldRelationship2.isEmpty
        {
            return false
        } else if textFieldName2.isEmpty || textFieldPhone2.isEmpty || textFieldRelationship2.isEmpty {
            return true
        }
        else {
            return false
            
        }
    }
    
    func stage3notfilled() -> Bool{
        if textFieldName3.isEmpty && textFieldPhone3.isEmpty && textFieldRelationship3.isEmpty
        {
            return false
        } else if textFieldName3.isEmpty || textFieldPhone3.isEmpty || textFieldRelationship3.isEmpty {
            return true
        }
        else {
            return false
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension  HipaaConsentVc: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldPhone1 || textField == textFieldPhone2 || textField == textFieldPhone3 {
            return textField.formatPhoneNumber(range, string: string)
            
        }
        return true
    }
    
}



/*
 // MARK: - Navigation
 
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


