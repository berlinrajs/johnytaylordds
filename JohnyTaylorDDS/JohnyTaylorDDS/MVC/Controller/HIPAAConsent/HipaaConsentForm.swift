//
//  HipaaConsentForm.swift
//  JohnyTaylorDDS
//
//  Created by SRS Web Solutions on 31/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class HipaaConsentForm: PDViewController {
    @IBOutlet var imageViewInitial1: UIImageView!
    @IBOutlet var imageViewInitial2: UIImageView!
    @IBOutlet var imageViewInitial3: UIImageView!
    @IBOutlet var imageViewInitial4: UIImageView!
    
    @IBOutlet var textFieldName1: UILabel!
    @IBOutlet var textFieldName2: UILabel!
    @IBOutlet var textFieldName3: UILabel!
    
    @IBOutlet var textFieldPhone1: UILabel!
    @IBOutlet var textFieldPhone2: UILabel!
    @IBOutlet var textFieldPhone3: UILabel!
    
    @IBOutlet var arrayButton: [UIButton]!
    
    @IBOutlet var textFieldRelationship1: UILabel!
    @IBOutlet var textFieldRelationship2: UILabel!
    @IBOutlet var textFieldRelationship3: UILabel!
    
    
    @IBOutlet var imageViewSignaturePatient: UIImageView!
    
    @IBOutlet var imageViewParent: UIImageView!
    
    @IBOutlet var labelDate: UILabel!
    
    @IBOutlet var labelDate2: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.text = patient.dateToday
        labelDate2.text = patient.dateToday
        for btn in arrayButton{
            btn.selected = patient.arrayHipaa.containsObject(btn.tag)
            
        }
       imageViewInitial1.image = patient.hipaaInitial1
        imageViewInitial2.image = patient.hipaaInitial2
        imageViewInitial3.image = patient.hipaaInitial3
        imageViewInitial4.image = patient.hipaaInitial4
        textFieldName1.text = patient.hipaaName1
        textFieldName2.text = patient.hipaaName2
        textFieldName3.text = patient.hipaaName3
        textFieldPhone1.text = patient.hipaaphone1
         textFieldPhone2.text = patient.hipaaPhone2
         textFieldPhone3.text = patient.hipaaPhone3
        textFieldRelationship1.text = patient.hipaaRelationShip1
        textFieldRelationship2.text = patient.hipaaRelationShip2
        textFieldRelationship3.text = patient.hipaaRelationShip3
        imageViewSignaturePatient.image = patient.signature1
        imageViewParent.image = patient.signature2

        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
