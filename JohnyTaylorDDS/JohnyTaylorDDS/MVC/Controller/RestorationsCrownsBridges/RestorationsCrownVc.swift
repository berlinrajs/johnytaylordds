//
//  RestorationsCrownVc.swift
//  JohnyTaylorDDS
//
//  Created by SRS Web Solutions on 11/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RestorationsCrownVc: PDViewController {
    
    @IBOutlet var textViewComments: PDTextView!
    @IBOutlet var imageViewPatientSignature: SignatureView!
    @IBOutlet var imageViewDoctorSignature: UIImageView!
    @IBOutlet var imageViewWitnessSignature: SignatureView!
    @IBOutlet var imageViewPatientInitial: SignatureView!
    @IBOutlet var labelDate1: DateLabel!
    @IBOutlet var labelDate2: DateLabel!
    @IBOutlet var labelDate3: DateLabel!
    var arrayMultiSelectOption2 : NSMutableArray!
    
    @IBOutlet var arrayButton: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday
        imageViewDoctorSignature.image = patient.doctorSign
        
        loadValues()
    }
    
    func loadValues() {
        self.arrayMultiSelectOption2 = patient.arrayRestorationsQuestion2 == nil ? NSMutableArray() : patient.arrayRestorationsQuestion2.mutableCopy() as! NSMutableArray
        self.textViewDidBeginEditing(textViewComments)
        textViewComments.text = patient.textOthers == nil ? "TYPE HERE" : patient.textOthers
        self.textViewDidEndEditing(textViewComments)
        
        for button in arrayButton {
            button.selected = self.arrayMultiSelectOption2.containsObject(button.tag)
        }
    }
    
    func saveValues() {
        patient.arrayRestorationsQuestion2 = self.arrayMultiSelectOption2
        patient.initialsImage = imageViewPatientInitial.signatureImage()
        patient.patientSignature = imageViewPatientSignature.signatureImage()
        patient.witnessSignature = imageViewWitnessSignature.signatureImage()
        self.textViewDidBeginEditing(textViewComments)
        patient.textOthers = textViewComments.text == "TYPE HERE" ? "" : textViewComments.text
        self.textViewDidEndEditing(textViewComments)
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }
    
    @IBAction func actionMultiselect2(sender: UIButton) {
        sender.selected = !sender.selected
        if arrayMultiSelectOption2.containsObject(sender.tag) {
            arrayMultiSelectOption2.removeObject(sender.tag)
        } else {
            arrayMultiSelectOption2.addObject(sender.tag)
        }
        if sender.tag == 7  && sender.selected == true
        {
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE TYPE HERE") { (textView, isEdited) in
                if isEdited {
                    self.patient.popUpText = textView.text
                } else {
                    sender.selected = false
                    self.arrayMultiSelectOption2.removeObject(sender.tag)
                    self.patient.popUpText = ""
                }
            }
        }
        else{
            
            self.patient.popUpText = ""
        }
    }
    
    @IBAction func buttonActionDone(sender: AnyObject) {
        if !imageViewPatientSignature.isSigned() ||  !imageViewWitnessSignature.isSigned() || !imageViewPatientInitial.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            
            saveValues()
            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("RestorationsCrownsForm") as! RestorationsCrownsForm
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension RestorationsCrownVc : UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "TYPE HERE" || textView.text == "" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

