//
//  EndodonticConsentVc.swift
//  JohnyTaylorDDS
//
//  Created by SRS Web Solutions on 11/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EndodonticConsentVc: PDViewController {

    @IBOutlet var imageViewPatientSignature: SignatureView!
    @IBOutlet var imageViewDoctorSignature: UIImageView!
    @IBOutlet var imageViewWitnessSignature: SignatureView!
    @IBOutlet var imageViewPatientInitial: SignatureView!
    @IBOutlet var labelDate1: DateLabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        imageViewDoctorSignature.image = patient.doctorSign
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        
        if !imageViewPatientSignature.isSigned() || !imageViewWitnessSignature.isSigned() || !imageViewPatientInitial.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !labelDate1.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("EndodonticConsentForm") as! EndodonticConsentForm
            patient.patientSignature = imageViewPatientSignature.signatureImage()
            patient.initialsImage = imageViewPatientInitial.signatureImage()
            patient.doctorSignature = imageViewDoctorSignature.image
            patient.witnessSignature = imageViewWitnessSignature.signatureImage()
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
    }
}
