//
//  EndodonticConsentForm.swift
//  JohnyTaylorDDS
//
//  Created by SRS Web Solutions on 11/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EndodonticConsentForm: PDViewController {

    
    @IBOutlet var imageViewPatientSignature: UIImageView!
    @IBOutlet var imageViewDoctorSignature: UIImageView!
    @IBOutlet var imageViewWitnessSignature: UIImageView!
    @IBOutlet var imageViewPatientInitial: UIImageView!
    @IBOutlet var labelDate1: FormLabel!
    
    
    @IBOutlet var labelPatientName: FormLabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.text = patient.dateToday
        imageViewPatientSignature.image = patient.patientSignature
        imageViewDoctorSignature.image = patient.doctorSign
        imageViewWitnessSignature.image = patient.witnessSignature
        imageViewPatientInitial.image = patient.initialsImage
        labelPatientName.text = patient.fullName
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
}

}
