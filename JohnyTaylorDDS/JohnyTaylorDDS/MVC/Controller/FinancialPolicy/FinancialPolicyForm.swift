//
//  FinancialPolicyForm.swift
//  JohnyTaylorDDS
//
//  Created by SRS Web Solutions on 16/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class FinancialPolicyForm: PDViewController {
    
    @IBOutlet var imageViewSignature: UIImageView!
    
    @IBOutlet var labelPatientName: FormLabel!

    @IBOutlet var labelDate1: FormLabel!
    
    @IBOutlet var labelRelationshipToPatient: FormLabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewSignature.image = patient.patientSignature
        labelPatientName.text = patient.fullName
        labelDate1.text = patient.dateToday
        labelRelationshipToPatient.text = patient.relationshipName

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
