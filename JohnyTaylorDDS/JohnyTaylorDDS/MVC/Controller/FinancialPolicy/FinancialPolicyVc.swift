//
//  FinancialPolicyVc.swift
//  JohnyTaylorDDS
//
//  Created by SRS Web Solutions on 16/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class FinancialPolicyVc: PDViewController {
    @IBOutlet var imageViewPatientSignature: SignatureView!
    @IBOutlet var labelDate1: DateLabel!
    @IBOutlet var textFieldRelationship: PDTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday

        // Do any additional setup after loading the view.
        loadValues()
    }
    
    func loadValues() {
        textFieldRelationship.text = patient.relationshipName
    }
    
    func saveValues() {
        patient.patientSignature = imageViewPatientSignature.image
        patient.relationshipName = textFieldRelationship.text
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }

    @IBAction func buttonActionNext(sender: AnyObject) {
        if !imageViewPatientSignature.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate1.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            saveValues()
            
            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("FinancialPolicyForm") as! FinancialPolicyForm
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
