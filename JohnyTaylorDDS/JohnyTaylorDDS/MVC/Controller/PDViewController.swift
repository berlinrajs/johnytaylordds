//
//  PDViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

let mainStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
let consentStoryBoard : UIStoryboard = UIStoryboard(name: "Consent", bundle: NSBundle.mainBundle())
let signInStoryBoard : UIStoryboard = UIStoryboard(name: "AdultMedicalHX", bundle: NSBundle.mainBundle())


class PDViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: PDButton?
    @IBOutlet var buttonBack: PDButton?
    
    @IBOutlet var pdfView: UIScrollView?
    
    var patient: PDPatient!
    
    var isFromPreviousForm: Bool {
        get {
            #if AUTO
                if navigationController?.viewControllers.count > 3 && navigationController?.viewControllers[2].isKindOfClass(VerificationViewController) == true {
                    navigationController?.viewControllers.removeAtIndex(2)
                    return false
                }
            #endif
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }

    func showAlert(message: String) {
        let alertController = UIAlertController(title: "Johny Taylor DDS", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Destructive) { (action) -> Void in
        }
        alertController.addAction(alertOkAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    func showAlert(message: String, completion: (completed: Bool) -> Void) {
        let alertController = UIAlertController(title: "Johny Taylor DDS", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Destructive) { (action) -> Void in
            completion(completed: true)
        }
        alertController.addAction(alertOkAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = .None
        buttonBack?.hidden = isFromPreviousForm && buttonSubmit == nil
        buttonSubmit?.backgroundColor = UIColor.greenColor()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonBackAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onSubmitButtonPressed (sender : UIButton){
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "Johny Taylor DDS", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
            let alertOkAction = UIAlertAction(title: "SETTINGS", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        let pdfManager = PDFManager()
        pdfManager.authorizeDrive(self.view) { (success) -> Void in
            if success {
                self.buttonSubmit?.hidden = true
                self.buttonBack?.hidden = true
                if self.pdfView != nil {
                    pdfManager.createPDFForScrollView(self.pdfView!, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            if self.patient.selectedForms.first!.formTitle! == "NOTICE OF PRIVACY PRACTICES"{
                                self.patient.selectedForms.removeFirst()
                            }
                            self.gotoNextForm(false)
                        } else {
                            self.buttonSubmit?.hidden = false
                            self.buttonBack?.hidden = false
                        }
                    })
                } else {
                    pdfManager.createPDFForView(self.view, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            self.gotoNextForm(false)
                        } else {
                            self.buttonSubmit?.hidden = false
                            self.buttonBack?.hidden = false
                        }
                    })
                }
                
            } else {
                self.buttonSubmit?.hidden = false
                self.buttonBack?.hidden = false
            }
        }
    }
    
    func gotoNextForm(showBackButton : Bool) {
        if isFromPreviousForm {
            if patient.selectedForms.count > 0 { patient.selectedForms.removeFirst() }
        }
        let formNames = (patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
        if formNames.contains(kNewPatientSignInForm) || formNames.contains(kPatientSignInForm) {
            let newPatientStep1VC = signInStoryBoard.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep1VCRef") as! PatientRegistrationAdultStep1VC
            newPatientStep1VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep1VC, animated: true)

        }else if formNames.contains(kInsuranceCard) {
            let cardCapture = mainStoryBoard.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            self.navigationController?.pushViewController(cardCapture, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let cardCapture = mainStoryBoard.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            cardCapture.isDrivingLicense = true
            self.navigationController?.pushViewController(cardCapture, animated: true)
        }else if formNames.contains(kToothWhitening){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("ToothWhitening1VC") as! ToothWhitening1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        else if formNames.contains(kNitrousOxide){
            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("NitrousOxideConsentVc") as! NitrousOxideConsentVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kPeriodontalCleaning){
                let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("PeriodontalCleaningVc") as! PeriodontalCleaningVc
                new1VC.patient = self.patient
                self.navigationController?.pushViewController(new1VC, animated: true)
        }
        else if formNames.contains(kEndodonticCleaning){
            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("EndodonticConsentVc") as! EndodonticConsentVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kMedicare){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("Medicare1VC") as! Medicare1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kCommunication){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("Communication1VC") as! Communication1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kRestorationsCrowns) {
            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("RestorationsCrownVc") as! RestorationsCrownVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(KCompositeFillings) {
        let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("CompositeFillingsVc") as! CompositeFillingsVc
        new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kToothRemoval) {
            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("TeethRemovalStep1VC") as! TeethRemovalStep1VC
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        else if formNames.contains(kFinancialPolicy) {
            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("FinancialPolicyVc") as! FinancialPolicyVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(KHippa) {
            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("HipaaConsentVc") as! HipaaConsentVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kFeedBack) {
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kFeedBackInfoViewController") as! FeedBackInfoViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
      else {
            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
            self.navigationController?.popToRootViewControllerAnimated(true)
       }
    }
}
