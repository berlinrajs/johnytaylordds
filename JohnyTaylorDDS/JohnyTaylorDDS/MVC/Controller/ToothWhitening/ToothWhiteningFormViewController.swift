//
//  ToothWhiteningFormViewController.swift
//  JohnyTaylorDDS
//
//  Created by Bala Murugan on 8/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ToothWhiteningFormViewController: PDViewController {

    var dictDetails : NSDictionary!
    @IBOutlet weak var Initial1 : UIImageView!
    @IBOutlet weak var Initial2 : UIImageView!
    @IBOutlet weak var Initial3 : UIImageView!
    @IBOutlet weak var imageViewPatientSign : UIImageView!
    @IBOutlet weak var imageViewDoctorSign : UIImageView!

    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelDate3 : UILabel!
    @IBOutlet weak var labelDate4 : UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()

        Initial1.image = dictDetails["Initial"] as? UIImage
        Initial2.image = dictDetails["Initial"] as? UIImage
        Initial3.image = dictDetails["Initial"] as? UIImage
        imageViewPatientSign.image = dictDetails["PatientSign"] as? UIImage
        imageViewDoctorSign.image = patient.doctorSign
        labelPatientName.text = patient.fullName
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday
        labelDate4.text = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
