//
//  ToothWhitening1ViewController.swift
//  JohnyTaylorDDS
//
//  Created by Bala Murugan on 8/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ToothWhitening1ViewController: PDViewController {
    @IBOutlet weak var patientInitial : SignatureView!
    @IBOutlet weak var patientSignature : SignatureView!
    @IBOutlet weak var dentistSignature : UIImageView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        dentistSignature.image = patient.doctorSign
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if !patientInitial.isSigned() || !patientSignature.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)

        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)

        }else{
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("ToothWhiteningFormVC") as! ToothWhiteningFormViewController
            new1VC.dictDetails = ["Initial" : patientInitial.signatureImage() , "PatientSign" : patientSignature.signatureImage()]
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)

        }
    }
}
