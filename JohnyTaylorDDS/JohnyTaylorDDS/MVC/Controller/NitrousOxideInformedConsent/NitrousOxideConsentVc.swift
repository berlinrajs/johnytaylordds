//
//  NitrousOxideConsentVc.swift
//  JohnyTaylorDDS
//
//  Created by SRS Web Solutions on 11/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NitrousOxideConsentVc: PDViewController {
 
    @IBOutlet var imageViewPatientSignature: SignatureView!
    @IBOutlet var imageViewDoctorSignature: UIImageView!
    @IBOutlet var imageViewWitnessSignature: SignatureView!
    @IBOutlet var labelDate1: DateLabel!
    @IBOutlet var labelDate2: DateLabel!
    @IBOutlet var labelDate3: DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday
        imageViewDoctorSignature.image = patient.doctorSign

        // Do any additional setup after loading the view.
    }

    
    @IBAction func buttonActionNext(sender: AnyObject) {
    
    if !imageViewPatientSignature.isSigned() || !imageViewWitnessSignature.isSigned(){
    let alert = Extention.alert("PLEASE SIGN THE FORM")
    self.presentViewController(alert, animated: true, completion: nil)
    }else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped{
    let alert = Extention.alert("PLEASE SELECT THE DATE")
    self.presentViewController(alert, animated: true, completion: nil)
    }else{
    let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("NitrousOxideForm") as! NitrousOxideForm
        patient.patientSignature = imageViewPatientSignature.image
        patient.doctorSignature = imageViewDoctorSignature.image
        patient.witnessSignature = imageViewWitnessSignature.image
      new1VC.patient = self.patient
       self.navigationController?.pushViewController(new1VC, animated: true)
    
    }
}



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
