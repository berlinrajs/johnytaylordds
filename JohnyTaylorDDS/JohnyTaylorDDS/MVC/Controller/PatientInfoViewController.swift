//
//  PatientInfoViewController.swift
//  ProDental
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: PDViewController {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldInitial: PDTextField!
    @IBOutlet var toolBar: UIToolbar!

    @IBOutlet weak var textfieldMonth : UITextField!
    @IBOutlet weak var textfieldDate : UITextField!
    @IBOutlet weak var textfieldYear : UITextField!
    @IBOutlet weak var pickerMonth : UIPickerView!
    var arrayMonths = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"]

    @IBOutlet weak var buttonNext: PDButton!
    
    var isNewPatient: Bool!
    var manager: AFHTTPSessionManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pickerMonth.dataSource = self
        pickerMonth.delegate = self
        textfieldMonth.inputView = pickerMonth
        textfieldMonth.inputAccessoryView = toolBar


        labelDate.text = patient.dateToday
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(sender : AnyObject) {
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER FIRST NAME")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER LAST NAME")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if textfieldDate.isEmpty || textfieldMonth.isEmpty || textfieldYear.isEmpty{
            let alert = Extention.alert("PLEASE ENTER DATE OF BIRTH")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if invalidDateofBirth{
            let alert = Extention.alert("PLEASE ENTER THE VALID DATE OF BIRTH")
            self.presentViewController(alert, animated: true, completion: nil)
            
        } else {
            patient.firstName = textFieldFirstName.text
            patient.lastName = textFieldLastName.text
            patient.dateOfBirth = textfieldMonth.text! + " " + textfieldDate.text! + ", " + textfieldYear.text!
            #if AUTO
                func showMoreThanOneUserAlert()  {
                    let alertController = UIAlertController(title: "Johny Taylor DDS", message: "More than one user found. Please handover the device to front desk to enter your patient id", preferredStyle: UIAlertControllerStyle.Alert)
                    let alertYesAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) -> Void in
                        let newPatientStep1VC = self.storyboard?.instantiateViewControllerWithIdentifier("VerificationVC") as! VerificationViewController
                        newPatientStep1VC.patient = self.patient
                        self.navigationController?.pushViewController(newPatientStep1VC, animated: true)
                    }
                    alertController.addAction(alertYesAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                    
                    
                }
                
                func showUnableToFindAlert (){
                    let alertController = UIAlertController(title: "Johny Taylor DDS", message: "Unable to find the patient \(textFieldFirstName.text!) \(textFieldLastName.text!) - \(patient.dateOfBirth) mismatch", preferredStyle: UIAlertControllerStyle.Alert)
                    let alertYesAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) -> Void in
                        showCreateNewPatientAlert()
                    }
                    alertController.addAction(alertYesAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                    
                }
                
                func showCreateNewPatientAlert (){
                    let alertController = UIAlertController(title: "Johny Taylor DDS", message: "Do you wish to create a new patient", preferredStyle: UIAlertControllerStyle.Alert)
                    let alertYesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (action) -> Void in
                        self.patient.patientDetails = nil
                        gotoPatientSignInForm()
                        
                    }
                    alertController.addAction(alertYesAction)
                    let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                        self.navigationController?.popToRootViewControllerAnimated(true)
                        
                    }
                    alertController.addAction(alertNoAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                    
                    
                }
                
                func gotoPatientSignInForm() {
                    let newPatientStep1VC = signInStoryBoard.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep1VCRef") as! PatientRegistrationAdultStep1VC
                    newPatientStep1VC.patient = self.patient
                    self.navigationController?.pushViewController(newPatientStep1VC, animated: true)
                }
                
                func APICall() {
                    BRProgressHUD.show()
                    self.buttonNext.userInteractionEnabled = false
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "MMM dd, yyyy"
                    let date = dateFormatter.dateFromString(patient.dateOfBirth)
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    manager = AFHTTPSessionManager(baseURL: NSURL(string: hostUrl))
                    manager?.responseSerializer.acceptableContentTypes = ["text/html"]
                    manager?.POST("consent_fetch_patient_info.php", parameters: ["first_name" : self.textFieldFirstName.text!, "last_name": self.textFieldLastName.text!, "dob": dateFormatter.stringFromDate(date!)], progress: { (progress) in
                        }, success: { (task, result) in
                            self.buttonNext.userInteractionEnabled = true
                            BRProgressHUD.hide()
                            if self.navigationController?.topViewController == self {
                                let response = result as! [String : AnyObject]
                                if response["status"] as! String == "success"  {
                                    let patientDetails = response["patientData"] as! [String: AnyObject]
                                    self.patient.patientDetails = PatientDetails(details: patientDetails)
                                    self.gotoNextForm(true)
                                } else {
                                    if response["status"] as! String == "failed" && (response["message"] as! String).containsString("Not connected") {
                                        let alert = Extention.alert((response["message"] as! String).uppercaseString)
                                        self.presentViewController(alert, animated: true, completion: nil)
                                    } else if response["status"] as! String == "matching_name"  {
                                        let patientDetails = response["matchingData"] as! [String: String]
                                        let alertController = UIAlertController(title: "Johny Taylor DDS", message: "Did you mean \(patientDetails["Fname"]!) \(patientDetails["Lname"]!)? If so select YES and choose the correct date of birth", preferredStyle: UIAlertControllerStyle.Alert)
                                        let alertOkAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (action) -> Void in
                                            self.textFieldFirstName.text = patientDetails["Fname"]
                                            self.textFieldLastName.text = patientDetails["Lname"]
                                            self.textfieldDate.text = ""
                                            self.textfieldMonth.text = ""
                                            self.textfieldYear.text = ""
                                        }
                                        alertController.addAction(alertOkAction)
                                        let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                                            showUnableToFindAlert()
                                        }
                                        alertController.addAction(alertNoAction)
                                        self.presentViewController(alertController, animated: true, completion: nil)
                                    } else if response["status"] as! String == "multiple_patient_found" {
                                        showMoreThanOneUserAlert()
                                    } else {
                                        self.patient.patientDetails = nil
                                        let alertController = UIAlertController(title: "Johny Taylor DDS", message: "Patient not found. Are you sure the provided details are correct?", preferredStyle: UIAlertControllerStyle.Alert)
                                        let alertYesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (action) -> Void in
                                            showUnableToFindAlert()
                                        }
                                        alertController.addAction(alertYesAction)
                                        let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                                            
                                        }
                                        alertController.addAction(alertNoAction)
                                        self.presentViewController(alertController, animated: true, completion: nil)
                                    }
                                }
                            }
                        }, failure: { (task, error) in
                            self.buttonNext.userInteractionEnabled = true
                            BRProgressHUD.hide()
                            self.patient.patientDetails = nil
                            if self.navigationController?.topViewController == self {
                                let alert = Extention.alert(error.localizedDescription)
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                    })
                }
                
                if self.isNewPatient == true {
                    self.gotoNextForm(true)
                } else {
                    APICall()
                }
            #else
                self.gotoNextForm(true)
            #endif
        }
    }
    
    var invalidDateofBirth: Bool {
        get {
            if textfieldMonth.isEmpty || textfieldDate.isEmpty || textfieldYear.isEmpty {
                return true
            } else if Int(textfieldDate.text!)! == 0{
                return true
            } else if !textfieldYear.text!.isValidYear {
                return true
            } else {
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                
                let todayDate = dateFormatter.dateFromString(dateFormatter.stringFromDate(NSDate()))
                let currentDate = dateFormatter.dateFromString("\(textfieldDate.text!)-\(textfieldMonth.text!)-\(textfieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSinceDate(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func toolbarDoneButtonAction(sender: AnyObject) {
        textfieldMonth.resignFirstResponder()
        let string1 = arrayMonths[pickerMonth.selectedRowInComponent(0)]
        textfieldMonth.text = string1.substringWithRange(string1.startIndex ..< string1.startIndex.advancedBy(3))
    }
    
}


extension PatientInfoViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
//        if textField == textFieldInitial {
//            return textField.formatInitial(range, string: string)
//        }else
    if textField == textfieldDate{
            return textField.formatDate(range, string: string)
        }else if textField == textfieldYear{
            return textField.formatNumbers(range, string: string, count: 4)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension PatientInfoViewController : UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayMonths.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayMonths[row]
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let string1 = arrayMonths[row]
        textfieldMonth.text = string1.substringWithRange(string1.startIndex ..< string1.startIndex.advancedBy(3))
    }
}
