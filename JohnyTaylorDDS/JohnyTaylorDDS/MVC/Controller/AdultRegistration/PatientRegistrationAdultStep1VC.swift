//
//  PatientRegistrationAdultStep1VC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/15/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultStep1VC: PDViewController {
    @IBOutlet weak var radioButtonGender: RadioButton!
    @IBOutlet weak var radioButtonMaritialStatus: RadioButton!
     @IBOutlet weak var textFieldPatientNumber
    : PDTextField!
    @IBOutlet weak var textFieldAddressLine: PDTextField!
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldZipCode: PDTextField!
    @IBOutlet weak var textFieldPhone: PDTextField!
    @IBOutlet weak var textFieldSecurityNumber: PDTextField!
    @IBOutlet weak var textFieldCellPhone: PDTextField!
    @IBOutlet weak var textFieldEmail: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StateListView.addStateListForTextField(textFieldState)
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    func loadValues() {
        
        func loadVal() {
            textFieldAddressLine.text = patient.addressLine
            textFieldCity.text = patient.city
            textFieldState.text = patient.state == nil ? "TX" : patient.state
            textFieldPhone.text = patient.phoneNumber
            textFieldZipCode.text = patient.zipCode
            
            if let gender = patient.gender {
                radioButtonGender.setSelectedWithTag(gender.index)
            }
            if let maritalStatus = patient.maritialStatus {
                radioButtonMaritialStatus.setSelectedWithTag(maritalStatus.index)
            }
            textFieldSecurityNumber.text = patient.socialSecurityNumber
            textFieldEmail.text = patient.email
            textFieldCellPhone.text = patient.cellPhoneNumber
        }
        
        #if AUTO
            if let patientDetails = patient.patientDetails {
                if let gender = patient.gender {
                    radioButtonGender.setSelectedWithTag(gender.index)
                } else if let gender = patientDetails.gender {
                    radioButtonGender.setSelectedWithTag(gender.index)
                }
                if let maritalStatus = patient.maritialStatus {
                    radioButtonMaritialStatus.setSelectedWithTag(maritalStatus.index)
                } else if let maritalStatus = patientDetails.maritalStatus {
                    radioButtonMaritialStatus.setSelectedWithTag(maritalStatus.index)
                }
                textFieldCity.text = patient.city != nil ? patient.city : patientDetails.city
                textFieldEmail.text = patient.email != nil ? patient.email : patientDetails.email
                textFieldPhone.text = patient.phoneNumber != nil ? patient.phoneNumber : patientDetails.homePhone
                textFieldState.text = patient.state != nil ? patient.state : patientDetails.state
                textFieldZipCode.text = patient.zipCode != nil ? patient.zipCode : patientDetails.zipCode
                textFieldAddressLine.text = patient.addressLine != nil ? patient.addressLine : patientDetails.address
                textFieldCellPhone.text = patient.cellPhoneNumber != nil ? patient.cellPhoneNumber : patientDetails.cellPhone
                textFieldSecurityNumber.text = patient.socialSecurityNumber != nil ? patient.socialSecurityNumber : patientDetails.socialSecurityNumber
            } else {
                loadVal()
            }
        #else
            loadVal()
        #endif
    }
    
    func saveValues() {
        patient.addressLine = textFieldAddressLine.text!
        patient.city = textFieldCity.text!
        patient.state = textFieldState.text!
        patient.phoneNumber = textFieldPhone.text!
        patient.zipCode = textFieldZipCode.text!
        
        patient.gender = radioButtonGender.selectedButton == nil ? Gender(value: 0) : Gender(value: radioButtonGender.selectedButton.tag)
        patient.maritialStatus = radioButtonMaritialStatus.selectedButton == nil ? MaritalStatus(value: 0) : MaritalStatus(value: radioButtonMaritialStatus.selectedButton.tag)
        
        patient.socialSecurityNumber = textFieldSecurityNumber.text!
        patient.email = textFieldEmail.text!
        patient.cellPhoneNumber = textFieldCellPhone.text!
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldZipCode.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIPCODE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldPhone.isEmpty && !textFieldPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID HOME PHONE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldCellPhone.isEmpty && !textFieldCellPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID WORK PHONE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldSecurityNumber.isEmpty && textFieldSecurityNumber.text!.characters.count != 9 {
            let alert = Extention.alert("PLEASE ENTER VALID SOCIAL SECURITY NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        }  else if radioButtonGender.selectedButton == nil  {
            let alert = Extention.alert("PLEASE SELECT GENDER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if radioButtonMaritialStatus.selectedButton == nil  {
            let alert = Extention.alert("PLEASE SELECT YOUR MARITIAL STATUS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail {
            let alert = Extention.alert("PLEASE ENTER A VALID EMAIL")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let newPatientStep2VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep2VCRef") as! PatientRegistrationAdultStep2VC
            newPatientStep2VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep2VC, animated: true)
        }
        
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textFieldAddressLine, textFieldCity, textFieldState, textFieldZipCode]
        for textField in textFields {
            if textField.isEmpty {
                return textField
            }
        }
        return nil
    }
    
}

extension PatientRegistrationAdultStep1VC : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        } else if textField == textFieldSecurityNumber {
            return textField.formatNumbers(range, string: string, count: 9)
        } else if textField == textFieldPhone || textField == textFieldCellPhone{
            return textField.formatPhoneNumber(range, string: string)

        }
        return true
    }
}
