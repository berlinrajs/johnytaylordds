//
//  PatientRegistrationAdultFormVC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/17/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultFormVC: PDViewController {
    
    
//    @IBOutlet weak var buttonBack: PDButton!
//    @IBOutlet weak var buttonSubmit: PDButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var viewPage2: UIView!
    @IBOutlet var viewPage3: UIView!
    @IBOutlet weak var patientInfo: AdultPatientInformation!
    @IBOutlet weak var patientInfo2: AdultPatientInformationReactions!
    @IBOutlet weak var patientInfoProblems1: AdultPatientInformationProblems1!
    @IBOutlet weak var patientInfoProblems2: AdultPatientInformationProblems2!
    @IBOutlet weak var patientInfoDental1: AdultDentalInformation1!
    @IBOutlet weak var patientInfoDental2: AdultDentalInformation2!
    
    //PATIENT INFO
    @IBOutlet weak var labelPatientName: FormLabel!
    @IBOutlet weak var labelPatientDate: FormLabel!
    @IBOutlet weak var labelPatientBirthday: FormLabel!
    @IBOutlet weak var labelPatientNumber: FormLabel!
    @IBOutlet weak var labelPatientSocialSecurity: FormLabel!
    @IBOutlet weak var labelPatientAddress: FormLabel!
    @IBOutlet weak var labelPatientCity: FormLabel!
    @IBOutlet weak var labelPatientState: FormLabel!
    @IBOutlet weak var labelPatientZipcode: FormLabel!
    @IBOutlet weak var labelPatientEmail: FormLabel!
    @IBOutlet weak var labelPatientHomePhone: FormLabel!
    @IBOutlet weak var labelPatientCellPhone: FormLabel!
    @IBOutlet weak var labelPatientEmployer: FormLabel!
    @IBOutlet weak var labelPatientEmployerPhone: FormLabel!
    @IBOutlet weak var labelPatientBusinessAddress: FormLabel!
    @IBOutlet weak var labelPatientBusinessCity: FormLabel!
    @IBOutlet weak var labelPatientBusinessState: FormLabel!
    @IBOutlet weak var labelPatientBusinessZipcode: FormLabel!
    @IBOutlet weak var labelPatientSpouseName: FormLabel!
    @IBOutlet weak var labelPatientSpouseEmployer: FormLabel!
    @IBOutlet weak var labelPatientSpouseWorkPhone: FormLabel!
    @IBOutlet weak var labelPatientSchool: FormLabel!
    @IBOutlet weak var labelPatientSchoolCity: FormLabel!
    @IBOutlet weak var labelPatientSchoolState: FormLabel!
    @IBOutlet weak var labelPatientReferrer: FormLabel!
    @IBOutlet weak var labelPatientEmergency: FormLabel!
    @IBOutlet weak var labelPatientEmergencyPhone: FormLabel!
    @IBOutlet weak var radioButtonMaritalStatus: RadioButton!
    @IBOutlet weak var radioButtonStudyType: RadioButton!
    
    //RESPONSIBLE PARTY
    @IBOutlet weak var labelResponsibleName: FormLabel!
    @IBOutlet weak var labelResponsibleAddress: FormLabel!
    @IBOutlet weak var labelResponsibleCity: FormLabel!
    @IBOutlet weak var labelResponsibleState: FormLabel!
    @IBOutlet weak var labelResponsibleZipcode: FormLabel!
    @IBOutlet weak var labelResponsibleRelationship: FormLabel!
    @IBOutlet weak var labelResponsibleEmail: FormLabel!
    @IBOutlet weak var labelResponsibleHomePhone: FormLabel!
    @IBOutlet weak var labelResponsibleCellPhone: FormLabel!
    @IBOutlet weak var labelResponsibleDriverLicense: FormLabel!
    @IBOutlet weak var labelResponsibleBirthday: FormLabel!
    @IBOutlet weak var labelResponsibleSocialSecurity: FormLabel!
    @IBOutlet weak var labelResponsibleWorkPhone: FormLabel!
    @IBOutlet weak var labelResponsibleEmployer: FormLabel!
    @IBOutlet weak var labelResponsibleFinancialInstitution: FormLabel!
    @IBOutlet weak var radioButtonCurrentlyPatient: RadioButton!
    @IBOutlet weak var radioButtonPaymentMethod: RadioButton!
    
    //INSURANCE
    @IBOutlet weak var labelInsuranceName: FormLabel!
    @IBOutlet weak var labelInsuranceBirthdate: FormLabel!
    @IBOutlet weak var labelInsuranceRelationship: FormLabel!
    @IBOutlet weak var labelInsuranceSocialSecurity: FormLabel!
    @IBOutlet weak var labelInsuranceDateEmployed: FormLabel!
    @IBOutlet weak var labelInsuranceEmployer: FormLabel!
    @IBOutlet weak var labelInsuranceUnion: FormLabel!
    @IBOutlet weak var labelInsuranceWorkPhone: FormLabel!
    @IBOutlet weak var labelInsuranceEmployerAddress: FormLabel!
    @IBOutlet weak var labelInsuranceEmployerCity: FormLabel!
    @IBOutlet weak var labelInsuranceEmployerState: FormLabel!
    @IBOutlet weak var labelInsuranceEmployerZipcode: FormLabel!
    @IBOutlet weak var labelInsuranceCompany: FormLabel!
    @IBOutlet weak var labelInsuranceGroup: FormLabel!
    @IBOutlet weak var labelInsurancePolicyID: FormLabel!
    @IBOutlet weak var labelInsuranceCoAddress: FormLabel!
    @IBOutlet weak var labelInsuranceCoCity: FormLabel!
    @IBOutlet weak var labelInsuranceCoState: FormLabel!
    @IBOutlet weak var labelInsuranceCoZipcode: FormLabel!
    @IBOutlet weak var labelInsuranceDeductible: FormLabel!
    @IBOutlet weak var labelInsuranceUsed: FormLabel!
    @IBOutlet weak var labelInsuranceAnnualBenefit: FormLabel!
    @IBOutlet weak var radioButtonAdditionalInsurance: RadioButton!
    
    //SECONDARY INSURANCE
    @IBOutlet weak var labelSecondaryInsuranceName: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceBirthdate: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceRelationship: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceSocialSecurity: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceDateEmployed: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceEmployer: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceUnion: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceWorkPhone: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceEmployerAddress: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceEmployerCity: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceEmployerState: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceEmployerZipcode: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceCompany: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceGroup: FormLabel!
    @IBOutlet weak var labelSecondaryInsurancePolicyID: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceCoAddress: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceCoCity: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceCoState: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceCoZipcode: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceDeductible: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceUsed: FormLabel!
    @IBOutlet weak var labelSecondaryInsuranceAnnualBenefit: FormLabel!
    
    //PAGE2
    @IBOutlet weak var labelPhysicianName: FormLabel!
    @IBOutlet weak var labelOfficePhone: FormLabel!
    @IBOutlet weak var labelDateLastExam: FormLabel!
    @IBOutlet weak var radioButtonQuestion10: RadioButton!
    @IBOutlet weak var radioButtonQuestion12: RadioButton!
    @IBOutlet weak var radioPregnent: RadioButton!
    @IBOutlet weak var radioNursing: RadioButton!
    @IBOutlet weak var radioOralContra: RadioButton!
    
    //DENATL INFO
    @IBOutlet weak var labelDentalPhysicianName: FormLabel!
    @IBOutlet weak var labelDentalDateLastExam: FormLabel!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelDateToday: FormLabel!
    
    //Doctors Comments
   
    @IBOutlet weak var imageViewSignature2: UIImageView!
    @IBOutlet weak var labelDateToday2: FormLabel!
    
    @IBOutlet var labelArray: [FormLabel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pdfView = self.scrollView
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let frame = CGRectMake(0, 1024, screenSize.width, screenSize.height)
        viewPage2.frame = frame
        scrollView.contentSize = CGSizeMake(screenSize.width, screenSize.height * 2)
        scrollView.addSubview(viewPage2)
        
        let frame1 = CGRectMake(0, 2048, screenSize.width, screenSize.height)
        viewPage3.frame = frame1
        scrollView.contentSize = CGSizeMake(screenSize.width, screenSize.height * 3)
        scrollView.addSubview(viewPage3)

    }
    
    func loadValues() {
        
        patientInfo.arrayOfQuestions = patient.patientInformation.questionGroup1
        patientInfo2.arrayOfQuestions = patient.patientInformation.questionGroup3
        
        var arrayOfQuestions = [PDQuestion]()
        arrayOfQuestions.appendContentsOf(patient.patientInformation.questionGroup2[0])
        arrayOfQuestions.appendContentsOf(patient.patientInformation.questionGroup2[1])
        
        patientInfoProblems1.arrayOfQuestions1 = Array(arrayOfQuestions[0...12])
        patientInfoProblems1.arrayOfQuestions2 = Array(arrayOfQuestions[13...25])
        patientInfoProblems2.arrayOfQuestions = Array(arrayOfQuestions[26...38])
        
        
        let array = ["", "Clicking", "Pain (joint, ear, side of face)", "Difficulty in opening or closing", "Difficulty in chewing"]
        let additionalQuestions = PDQuestion.arrayOfQuestions(array)
        additionalQuestions[1].selectedOption = patient.dentalInformation.clicking == 1
        additionalQuestions[2].selectedOption = patient.dentalInformation.pain == 1
        additionalQuestions[3].selectedOption = patient.dentalInformation.openOrClose == 1
        additionalQuestions[4].selectedOption = patient.dentalInformation.chewing == 1
        
        var arrayQuestions = patient.dentalInformation.questionGroup1
        arrayQuestions.appendContentsOf(additionalQuestions)
        patientInfoDental1.arrayQuestions = arrayQuestions
        
        patientInfoDental2.arrayQuestions = patient.dentalInformation.questionGroup2
        
        //PatientInfo
        labelPatientNumber.text = patient.patientNumber
        labelPatientName.text = patient.fullName
        labelPatientDate.text = patient.dateToday
        labelPatientBirthday.text = patient.dateOfBirth
        labelPatientSocialSecurity.text = patient.socialSecurityNumber!.socialSecurityNumber
        labelPatientAddress.text = patient.addressLine
        labelPatientCity.text = patient.city
        labelPatientState.text = patient.state
        labelPatientZipcode.text = patient.zipCode
        labelPatientEmail.text = patient.email
        labelPatientHomePhone.text = patient.phoneNumber
        labelPatientCellPhone.text = patient.cellPhoneNumber
        labelPatientEmployer.text = patient.employerName!.isEmpty ? "N/A" : patient.employerName
        labelPatientEmployerPhone.text = patient.employerPhone!.isEmpty ? "N/A" : patient.employerPhone
        labelPatientBusinessAddress.text = patient.employerAddress!.isEmpty ? "N/A" : patient.employerAddress
        labelPatientBusinessCity.text = patient.employerCity!.isEmpty ? "N/A" : patient.employerCity
        labelPatientBusinessState.text = patient.employerState!.isEmpty ? "N/A" : patient.employerState
        labelPatientBusinessZipcode.text = patient.employerZip!.isEmpty ? "N/A" : patient.employerZip
        labelPatientSpouseName.text = patient.spouseName!.isEmpty ? "N/A" : patient.spouseName
        labelPatientSpouseEmployer.text = patient.spouseEmployerName!.isEmpty ? "N/A" : patient.spouseEmployerName
        labelPatientSpouseWorkPhone.text = patient.spouseWorkPhone!.isEmpty ? "N/A" : patient.spouseWorkPhone
        labelPatientSchool.text = patient.schoolName
        labelPatientSchoolCity.text = patient.schoolCity
        labelPatientSchoolState.text = patient.schoolState
        labelPatientReferrer.text = patient.referredBy
        labelPatientEmergency.text = patient.emergencyContactName
        labelPatientEmergencyPhone.text = patient.emergencyContactPhoneNumber
        radioButtonMaritalStatus.setSelectedWithTag(patient.maritialStatus.index)
        if patient.studyType == nil {
            radioButtonStudyType.deselectAllButtons()
        } else {
            radioButtonStudyType.setSelectedWithTag(patient.studyType!)
        }
        
        //ResponsibleParty
        labelResponsibleName.text = patient.responsibleparty.personName.isEmpty ? "N/A" : patient.responsibleparty.personName
        labelResponsibleAddress.text = patient.responsibleparty.address!.isEmpty ? "N/A" : patient.responsibleparty.address
        labelResponsibleCity.text = patient.responsibleparty.city!.isEmpty ? "N/A" : patient.responsibleparty.city
        labelResponsibleState.text = patient.responsibleparty.state!.isEmpty ? "N/A" : patient.responsibleparty.state
        labelResponsibleZipcode.text = patient.responsibleparty.zip!.isEmpty ? "N/A" : patient.responsibleparty.zip
        labelResponsibleRelationship.text = patient.responsibleparty.reloationShipToPatient
        labelResponsibleEmail.text = patient.responsibleparty.email!.isEmpty ? "N/A" : patient.responsibleparty.email
        labelResponsibleHomePhone.text = patient.responsibleparty.homePhone!.isEmpty ? "N/A" : patient.responsibleparty.homePhone
        labelResponsibleCellPhone.text = patient.responsibleparty.cellPhone!.isEmpty ? "N/A" : patient.responsibleparty.personName
        labelResponsibleDriverLicense.text = patient.responsibleparty.driverLicence!.isEmpty ? "N/A" : patient.responsibleparty.driverLicence
        labelResponsibleBirthday.text = patient.responsibleparty.birthDate!.isEmpty ? "N/A" : patient.responsibleparty.birthDate
        labelResponsibleSocialSecurity.text = patient.responsibleparty.socialSecurity!.isEmpty ? "N/A" : patient.responsibleparty.socialSecurity?.socialSecurityNumber
        labelResponsibleWorkPhone.text = patient.responsibleparty.workPhone!.isEmpty ? "N/A" : patient.responsibleparty.workPhone
        labelResponsibleEmployer.text = patient.responsibleparty.employer!.isEmpty ? "N/A" : patient.responsibleparty.employer
        radioButtonCurrentlyPatient.setSelectedWithTag(patient.responsibleparty.isPatient!)
        
        if patient.primaryInsurance != nil {
            //PrimaryInsurance
            labelInsuranceName.text = patient.primaryInsurance.name!.isEmpty ? "N/A" : patient.primaryInsurance.name
            labelInsuranceBirthdate.text = patient.primaryInsurance.birthDate!.isEmpty ? "N/A" : patient.primaryInsurance.birthDate
            labelInsuranceRelationship.text = patient.primaryInsurance.relationShipToPatient!.isEmpty ? "N/A" : patient.primaryInsurance.relationShipToPatient
            labelInsuranceSocialSecurity.text = patient.primaryInsurance.socialSecurityNumber.isEmpty ? "N/A" : patient.primaryInsurance.socialSecurityNumber.socialSecurityNumber
            labelInsuranceDateEmployed.text = patient.primaryInsurance.dateEmployed.isEmpty ? "N/A" : patient.primaryInsurance.dateEmployed
            labelInsuranceEmployer.text = patient.primaryInsurance.nameOfEmployer.isEmpty ? "N/A" : patient.primaryInsurance.nameOfEmployer
            labelInsuranceUnion.text = patient.primaryInsurance.union.isEmpty ? "N/A" : patient.primaryInsurance.union
            labelInsuranceWorkPhone.text = patient.primaryInsurance.workPhone.isEmpty ? "N/A" : patient.primaryInsurance.workPhone
            labelInsuranceEmployerAddress.text = patient.primaryInsurance.address.isEmpty ? "N/A" : patient.primaryInsurance.address
            labelInsuranceEmployerCity.text = patient.primaryInsurance.city.isEmpty ? "N/A" : patient.primaryInsurance.city
            labelInsuranceEmployerState.text = patient.primaryInsurance.state.isEmpty ? "N/A" : patient.primaryInsurance.state
            labelInsuranceEmployerZipcode.text = patient.primaryInsurance.zip.isEmpty ? "N/A" : patient.primaryInsurance.zip
            labelInsuranceCompany.text = patient.primaryInsurance.insuranceCompName.isEmpty ? "N/A" : patient.primaryInsurance.insuranceCompName
            labelInsuranceGroup.text = patient.primaryInsurance.insuranceGroup.isEmpty ? "N/A" : patient.primaryInsurance.insuranceGroup
            labelInsurancePolicyID.text = patient.primaryInsurance.insurancePolicyID.isEmpty ? "N/A" : patient.primaryInsurance.insurancePolicyID
            labelInsuranceCoAddress.text = patient.primaryInsurance.insuranceCompanyAddress.isEmpty ? "N/A" : patient.primaryInsurance.insuranceCompanyAddress
            labelInsuranceCoCity.text = patient.primaryInsurance.insuranceCompanyCity.isEmpty ? "N/A" : patient.primaryInsurance.insuranceCompanyCity
            labelInsuranceCoState.text = patient.primaryInsurance.insuranceCompanyState.isEmpty ? "N/A" : patient.primaryInsurance.insuranceCompanyState
            labelInsuranceCoZipcode.text = patient.primaryInsurance.insuranceCompanyZip.isEmpty ? "N/A" : patient.primaryInsurance.insuranceCompanyZip
            labelInsuranceDeductible.text = patient.primaryInsurance.deductible.isEmpty ? "N/A" : patient.primaryInsurance.deductible
            labelInsuranceUsed.text = patient.primaryInsurance.used.isEmpty ? "N/A" : patient.primaryInsurance.used
            labelInsuranceAnnualBenefit.text = patient.primaryInsurance.maximumAnualBenifit.isEmpty ? "N/A" : patient.primaryInsurance.maximumAnualBenifit
            radioButtonAdditionalInsurance.setSelectedWithTag(patient.haveSecondaryInsurance)
        }
        
        if patient.secondaryInsurance != nil {
            labelSecondaryInsuranceName.text = patient.secondaryInsurance.name
            labelSecondaryInsuranceBirthdate.text = patient.secondaryInsurance.birthDate
            labelSecondaryInsuranceRelationship.text = patient.secondaryInsurance.relationShipToPatient
            labelSecondaryInsuranceSocialSecurity.text = patient.secondaryInsurance.socialSecurityNumber.socialSecurityNumber
            labelSecondaryInsuranceDateEmployed.text = patient.secondaryInsurance.dateEmployed
            labelSecondaryInsuranceEmployer.text = patient.secondaryInsurance.nameOfEmployer
            labelSecondaryInsuranceUnion.text = patient.secondaryInsurance.union
            labelSecondaryInsuranceWorkPhone.text = patient.secondaryInsurance.workPhone
            labelSecondaryInsuranceEmployerAddress.text = patient.secondaryInsurance.address
            labelSecondaryInsuranceEmployerCity.text = patient.secondaryInsurance.city
            labelSecondaryInsuranceEmployerState.text = patient.secondaryInsurance.state
            labelSecondaryInsuranceEmployerZipcode.text = patient.secondaryInsurance.zip
            labelSecondaryInsuranceCompany.text = patient.secondaryInsurance.insuranceCompName
            labelSecondaryInsuranceGroup.text = patient.secondaryInsurance.insuranceGroup
            labelSecondaryInsurancePolicyID.text = patient.secondaryInsurance.insurancePolicyID
            labelSecondaryInsuranceCoAddress.text = patient.secondaryInsurance.insuranceCompanyAddress
            labelSecondaryInsuranceCoCity.text = patient.secondaryInsurance.insuranceCompanyCity
            labelSecondaryInsuranceCoState.text = patient.secondaryInsurance.insuranceCompanyState
            labelSecondaryInsuranceCoZipcode.text = patient.secondaryInsurance.insuranceCompanyZip
            labelSecondaryInsuranceDeductible.text = patient.secondaryInsurance.deductible
            labelSecondaryInsuranceUsed.text = patient.secondaryInsurance.used
            labelSecondaryInsuranceAnnualBenefit.text = patient.secondaryInsurance.maximumAnualBenifit
        }
        
        //PAGE 2
        labelPhysicianName.text = patient.patientInformation.physicianName!.isEmpty ? "N/A" : patient.patientInformation.physicianName
        labelOfficePhone.text = patient.patientInformation.officePhone!.isEmpty ? "N/A" : patient.patientInformation.officePhone
        labelDateLastExam.text = patient.patientInformation.dateOfLastExam!.isEmpty ? "N/A" : patient.patientInformation.dateOfLastExam
        radioButtonQuestion10.setSelectedWithTag(patient.patientInformation.contactLense!)
        radioButtonQuestion12.setSelectedWithTag(patient.patientInformation.persistentCough!)
        
        if patient.patientInformation.isPregnent != nil {
            radioPregnent.setSelectedWithTag(patient.patientInformation.isPregnent!)
        } else {
            radioPregnent.deselectAllButtons()
        }
        
        if patient.patientInformation.isNursing != nil {
            radioNursing.setSelectedWithTag(patient.patientInformation.isNursing!)
        } else {
            radioNursing.deselectAllButtons()
        }
        
        if patient.patientInformation.oralContraceptives != nil {
            radioOralContra.setSelectedWithTag(patient.patientInformation.oralContraceptives!)
        } else {
            radioOralContra.deselectAllButtons()
        }
        
        //DentalInfo
        labelDentalPhysicianName.text = patient.dentalInformation.previousDentistNameAndLocation!.isEmpty ? "N/A" : patient.dentalInformation.previousDentistNameAndLocation
        labelDentalDateLastExam.text = patient.dentalInformation.dateOfLastExam!.isEmpty ? "N/A" : patient.dentalInformation.dateOfLastExam
        imageViewSignature.image = patient.patientInformation.patientOrGuardianSignature
        labelDateToday.text = patient.dateToday
        
        //Doctors comment
        imageViewSignature2.image = patient.doctorSign
        labelDateToday2.text = patient.dateToday
        patient.textOthers.setTextForArrayOfLabels(labelArray)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func onSubmitButtonPressed(sender: UIButton) {        
        #if AUTO
            if !Reachability.isConnectedToNetwork() {
                let alertController = UIAlertController(title: "Johny Taylor DDS", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.sharedApplication().openURL(url)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.presentViewController(alertController, animated: true, completion: nil)
                return
            }
            BRProgressHUD.show()
            ServiceManager.sendPatientDetails(patient, completion: { (success, error) in
                BRProgressHUD.hide()
                if success {
                    super.onSubmitButtonPressed(sender)
                } else {
                    let alert = Extention.alert(error!.localizedDescription.uppercaseString)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            })
        #else
            super.onSubmitButtonPressed(sender)
        #endif
    }
}
