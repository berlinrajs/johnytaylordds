//
//  PatientRegistrationAdultStep5VC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/16/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultStep5VC: PDViewController {

    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var textFieldLicense: PDTextField!
    @IBOutlet weak var textFieldBirthDate: PDTextField!
    @IBOutlet weak var textFieldEmployer: PDTextField!
    @IBOutlet weak var textFieldWorkPhone: PDTextField!
    @IBOutlet weak var radioButtonIsPatient: RadioButton!
    @IBOutlet weak var radioButtonPaymentMethod: RadioButton!
    @IBOutlet weak var radioButtonHaveInsurance: RadioButton!
    
    @IBOutlet var textFieldSocialSecurity: PDTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "MMMM dd, yyyy"
        self.datePicker.maximumDate = NSDate()
        datePicker.setDate(dateformatter.dateFromString("Jan 01 1980")!, animated: false)
        self.textFieldBirthDate.inputView = self.datePicker
        self.textFieldBirthDate.inputAccessoryView = self.toolbar
        
        loadValues()
        
        if patient.responsibleparty.resSelfButton == 1 {
            textFieldSocialSecurity.text = patient.socialSecurityNumber == "N/A" ? "" : patient.socialSecurityNumber
            textFieldBirthDate.text = patient.dateOfBirth
            textFieldEmployer.text = patient.employerName == "N/A" ? "" : patient.employerName
            textFieldWorkPhone.text = patient.employerPhone == "N/A" ? "" : patient.employerPhone
        } else {
            textFieldSocialSecurity.text = ""
            textFieldBirthDate.text = ""
            textFieldEmployer.text = ""
            textFieldWorkPhone.text = ""
        }
    }
    
    func loadValues() {
        textFieldLicense.text = patient.responsibleparty.driverLicence
        textFieldBirthDate.text = patient.responsibleparty.birthDate
        textFieldEmployer.text = patient.responsibleparty.employer
        textFieldWorkPhone.text = patient.responsibleparty.workPhone
        textFieldSocialSecurity.text = patient.responsibleparty.socialSecurity
        self.radioButtonIsPatient.setSelectedWithTag(patient.responsibleparty.isPatient == nil ? 0 : patient.responsibleparty.isPatient)
        self.radioButtonHaveInsurance.setSelectedWithTag(patient.haveInsurance == nil ? 0 : patient.haveInsurance!)
    }
    
    func saveValues() {
        patient.responsibleparty.driverLicence = textFieldLicense.text!
        patient.responsibleparty.birthDate = textFieldBirthDate.text!
        patient.responsibleparty.employer = textFieldEmployer.text!
        patient.responsibleparty.workPhone = textFieldWorkPhone.text!
        patient.responsibleparty.socialSecurity = textFieldSocialSecurity.text!
        patient.responsibleparty.isPatient = self.radioButtonIsPatient.selectedButton == nil ? 0 : self.radioButtonIsPatient.selectedButton.tag
        patient.haveInsurance = self.radioButtonHaveInsurance.selectedButton == nil ? 0 : self.radioButtonHaveInsurance.selectedButton.tag
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
         if !textFieldSocialSecurity.isEmpty && textFieldSocialSecurity.text!.characters.count != 9 {
            let alert = Extention.alert("PLEASE ENTER A VALID SOCIAL SECURITY NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if radioButtonIsPatient.selectedButton == nil  || self.radioButtonHaveInsurance.selectedButton == nil {
            let alert = Extention.alert("PLEASE ANSWER ALL THE MANDATORY QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldWorkPhone.isEmpty && !textFieldWorkPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER A VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            if patient.haveInsurance == 1 {
                let newPatientStep6VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep6VCRef") as! PatientRegistrationAdultStep6VC
                newPatientStep6VC.patient = self.patient
                newPatientStep6VC.isSecondaryInsurance = false
                self.navigationController?.pushViewController(newPatientStep6VC, animated: true)
            } else {
                let newPatientStep8VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep8VCRef") as! PatientRegistrationAdultStep8VC
                newPatientStep8VC.patient = self.patient
                self.navigationController?.pushViewController(newPatientStep8VC, animated: true)
            }
        }
    }
    @IBAction func toolbarDoneButtonAction(sender: AnyObject) {
        textFieldBirthDate.resignFirstResponder()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldBirthDate.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    
    @IBAction func datePickerDateChanged(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldBirthDate.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
}
extension PatientRegistrationAdultStep5VC : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldWorkPhone {
            return textField.formatPhoneNumber(range, string: string)
        }else if textField == textFieldSocialSecurity {
            return textField.formatNumbers(range, string: string, count: 9)
        }
        return true
    }
}
