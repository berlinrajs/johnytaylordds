//
//  PatientRegistrationAdultStep12VC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/17/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultStep12VC: PDViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var popUpView: PDView!
    @IBOutlet weak var textViewAnswer: PDTextView!
    @IBOutlet weak var viewShadow: UIView!
    
    @IBOutlet weak var datePopUpView: PDView!
    @IBOutlet weak var dateTextFieldAnswer: PDTextField!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var buttonAnswered: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.maximumDate = NSDate()
        dateTextFieldAnswer.inputView = datePicker
        dateTextFieldAnswer.inputAccessoryView = toolBar
        // Do any additional setup after loading the view.
        let dateString = "1 Jan \(NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: NSDate()))"
        let df = NSDateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.dateFromString(dateString)
        if let unwrappedDate = date {
            self.datePicker.setDate(unwrappedDate, animated: false)
        }
        
        loadValues()
    }
    
    func loadValues() {
        
    }

    func saveValues() {
        
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dateTextFieldAnswer.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    
    @IBAction func datePickerDonePressed(sender: AnyObject) {
        dateTextFieldAnswer.resignFirstResponder()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dateTextFieldAnswer.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    
    @IBAction func buttonActionAnswered(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if buttonAnswered.selected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if self.anyUnAnsweredQuestion {
            let alert = Extention.alert("PLEASE ANSWER ALL THE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            let newPatientStep13VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep13VCRef") as! PatientRegistrationAdultStep13VC
            newPatientStep13VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep13VC, animated: true)
        }
    }
    var anyUnAnsweredQuestion: Bool {
        get {
            for question in self.patient.dentalInformation.questionGroup2 {
                if(question.selectedOption == nil) {
                    return true
                }
            }
            return false
        }
    }
}
extension PatientRegistrationAdultStep12VC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    //    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    //        return 40
    //    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.dentalInformation.questionGroup2.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PatientInfoCell1", forIndexPath: indexPath) as! PatientInfoCell
        
        cell.delegate = self
        cell.configCell(self.patient.dentalInformation.questionGroup2[indexPath.row])
        
        return cell
    }
}
extension PatientRegistrationAdultStep12VC: PatientInfoCellDelegate {
    func radioButtonTappedForCell(cell: PatientInfoCell) {
        let indexPath = self.tableView.indexPathForCell(cell)!
        if indexPath.row == 8 {
//            showPopUp(indexPath.row)
        } else {
            showDatepopup(indexPath.row)
        }
    }
    
    func showPopUp(tag: Int) {
        textViewAnswer.text = "IF YES, TYPE HERE"
        textViewAnswer.textColor = UIColor.lightGrayColor()
        textViewAnswer.tag = tag
        self.popUpView.frame = CGRectMake(0, 0, 512.0, 250.0)
        self.popUpView.center = self.view.center
        self.viewShadow.addSubview(self.popUpView)
        self.popUpView.transform = CGAffineTransformMakeScale(0.1, 0.1)
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.popUpView.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    @IBAction func popupDonePressed(sender: AnyObject) {
        textViewAnswer.resignFirstResponder()
        
        self.popUpView.removeFromSuperview()
        self.viewShadow.hidden = true
        
        if textViewAnswer.isEmpty || textViewAnswer.text == "IF YES, TYPE HERE" {
            self.patient.dentalInformation.questionGroup2[textViewAnswer.tag].selectedOption = false
            self.tableView.reloadData()
        } else {
            self.patient.dentalInformation.questionGroup2[textViewAnswer.tag].answer = textViewAnswer.text!
        }
    }
    func showDatepopup(tag: Int) {
        
        dateTextFieldAnswer.tag = tag
        self.datePopUpView.frame = CGRectMake(0, 0, 512.0, 250.0)
        self.datePopUpView.center = self.view.center
        self.viewShadow.addSubview(self.datePopUpView)
        self.datePopUpView.transform = CGAffineTransformMakeScale(0.1, 0.1)
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.datePopUpView.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    @IBAction func datePopupDonePressed(sender: AnyObject) {
        dateTextFieldAnswer.resignFirstResponder()
        
        self.datePopUpView.removeFromSuperview()
        self.viewShadow.hidden = true
        
        if dateTextFieldAnswer.isEmpty {
            self.patient.dentalInformation.questionGroup2[dateTextFieldAnswer.tag].selectedOption = false
            self.tableView.reloadData()
        } else {
            self.patient.dentalInformation.questionGroup2[dateTextFieldAnswer.tag].answer = dateTextFieldAnswer.text!
        }
    }
}
extension PatientRegistrationAdultStep12VC: UITextViewDelegate {
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "IF YES, TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = "IF YES, TYPE HERE"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
