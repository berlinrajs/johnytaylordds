//
//  PatientRegistrationAdultStep9VC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/17/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultStep9VC: PDViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var popUpView: PDView!
    @IBOutlet weak var textViewAnswer: PDTextView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var buttonAnswered: UIButton!

    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    func loadValues() {
        
    }
    
    func saveValues() {
        
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
   @IBAction func buttonActionBack(sender: AnyObject) {
        if selectedIndex == 0 {
            saveValues()
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            selectedIndex = selectedIndex - 1
            self.tableView.reloadData()
        }
    }
    
    @IBAction func buttonActionAnswered(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if buttonAnswered.selected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if self.anyUnAnsweredQuestion {
            let alert = Extention.alert("PLEASE ANSWER ALL THE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            if selectedIndex == 1 {
                let newPatientStep10VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep10VCRef") as! PatientRegistrationAdultStep10VC
                newPatientStep10VC.patient = self.patient
                self.navigationController?.pushViewController(newPatientStep10VC, animated: true)
            } else if selectedIndex == 0 {
                self.buttonAnswered.selected = false
                self.selectedIndex = selectedIndex + 1
                self.tableView.reloadData()
            }
        }
    }
    var anyUnAnsweredQuestion: Bool {
        get {
            for question in self.patient.patientInformation.questionGroup2[selectedIndex] {
                if(question.selectedOption == nil) {
                    return true
                }
            }
            return false
        }
    }
}
extension PatientRegistrationAdultStep9VC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return 40
//    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.patientInformation.questionGroup2[selectedIndex].count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PatientInfoCell1", forIndexPath: indexPath) as! PatientInfoCell
        
        cell.delegate = self
        cell.configCell(self.patient.patientInformation.questionGroup2[selectedIndex][indexPath.row])
        
        return cell
    }
}
extension PatientRegistrationAdultStep9VC: PatientInfoCellDelegate {
    
    func radioButtonTappedForCell(cell: PatientInfoCell) {
        showPopUp(self.tableView.indexPathForCell(cell)!.row)
    }
  
    func showPopUp(tag: Int) {
        textViewAnswer.text = tag == 0 ? "IF YES, WHAT KIND?" : "WHAT ARE THEY? TYPE DETAILS HERE"
        textViewAnswer.textColor = UIColor.lightGrayColor()
        textViewAnswer.tag = tag
        self.popUpView.frame = CGRectMake(0, 0, 512.0, 250.0)
        self.popUpView.center = self.view.center
        self.viewShadow.addSubview(self.popUpView)
        self.popUpView.transform = CGAffineTransformMakeScale(0.1, 0.1)
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.popUpView.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    @IBAction func popupDonePressed(sender: AnyObject) {
        textViewAnswer.resignFirstResponder()
        
        self.popUpView.removeFromSuperview()
        self.viewShadow.hidden = true
        
        if textViewAnswer.isEmpty || textViewAnswer.text == "IF YES, WHAT KIND?" || textViewAnswer.text == "WHAT ARE THEY? TYPE DETAILS HERE" {
            self.patient.patientInformation.questionGroup2[selectedIndex][textViewAnswer.tag].selectedOption = false
            self.tableView.reloadData()
        } else {
            self.patient.patientInformation.questionGroup2[selectedIndex][textViewAnswer.tag].answer = textViewAnswer.text!
        }
    }
}
extension PatientRegistrationAdultStep9VC: UITextViewDelegate {
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "IF YES, WHAT KIND?" || textView.text == "WHAT ARE THEY? TYPE DETAILS HERE" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = textView.tag == 0 ? "IF YES, WHAT KIND?" : "WHAT ARE THEY? TYPE DETAILS HERE"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
