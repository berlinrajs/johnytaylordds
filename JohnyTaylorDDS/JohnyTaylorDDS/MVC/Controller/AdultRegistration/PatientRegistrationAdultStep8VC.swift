//
//  PatientRegistrationAdultStep8VC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/16/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultStep8VC: PDViewController {

    
    @IBOutlet weak var textFieldPhysician: PDTextField!
    @IBOutlet weak var textFieldPhoneNumber: PDTextField!
    @IBOutlet weak var textFieldLastExam: PDTextField!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var toolBar: UIToolbar!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var popUpView: PDView!
    @IBOutlet weak var textViewAnswer: PDTextView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var buttonAnswered: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.maximumDate = NSDate()
        textFieldLastExam.inputView = datePicker
        textFieldLastExam.inputAccessoryView = toolBar
        let dateString = "1 Jan \(NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: NSDate()))"
        let df = NSDateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.dateFromString(dateString)
        if let unwrappedDate = date {
            self.datePicker.setDate(unwrappedDate, animated: false)
        }
        
        loadValues()
    }
    
    func loadValues() {
//        if self.patient.patientInformation == nil {
//            self.patient.patientInformation = PatientInformation()
//        }
        self.textFieldPhysician.text = self.patient.patientInformation.physicianName
        self.textFieldPhoneNumber.text = self.patient.patientInformation.officePhone
        self.textFieldLastExam.text = self.patient.patientInformation.dateOfLastExam
        self.tableView.reloadData()
    }
    
    func saveValues() {
        self.patient.patientInformation.physicianName = self.textFieldPhysician.text!
        self.patient.patientInformation.officePhone = self.textFieldPhoneNumber.text!
        self.patient.patientInformation.dateOfLastExam = self.textFieldLastExam.text!
    }
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionAnswered(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldLastExam.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    
    @IBAction func datePickerDonePressed(sender: AnyObject) {
        textFieldLastExam.resignFirstResponder()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldLastExam.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    @IBAction func popupDonePressed(sender: AnyObject) {
        textViewAnswer.resignFirstResponder()
        
        self.popUpView.removeFromSuperview()
        self.viewShadow.hidden = true
        
        if textViewAnswer.isEmpty || textViewAnswer.text == "PLEASE EXPLAIN.TYPE DETAILS HERE" || textViewAnswer.text == "WHAT MEDICATIONS(S) ARE YOU TAKING? TYPE DETAILS HERE" {
            self.patient.patientInformation.questionGroup1[textViewAnswer.tag].selectedOption = false
            self.tableView.reloadData()
        } else {
            self.patient.patientInformation.questionGroup1[textViewAnswer.tag].answer = textViewAnswer.text!
        }
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if !textFieldPhoneNumber.isEmpty && !textFieldPhoneNumber.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER A VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if buttonAnswered.selected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if self.anyUnAnsweredQuestion {
            let alert = Extention.alert("PLEASE ANSWER ALL THE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let newPatientStep9VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep9VCRef") as! PatientRegistrationAdultStep9VC
            newPatientStep9VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep9VC, animated: true)
        }
    }
    var anyUnAnsweredQuestion: Bool {
        get {
            for question in self.patient.patientInformation.questionGroup1 {
                if(question.selectedOption == nil) {
                    return true
                }
            }
            return false
        }
    }
}
extension PatientRegistrationAdultStep8VC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 4 || indexPath.row == 5 ? 55 : 40
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.patientInformation.questionGroup1.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PatientInfoCell1", forIndexPath: indexPath) as! PatientInfoCell
        
        cell.delegate = self
        cell.configCellWithAsterix(self.patient.patientInformation.questionGroup1[indexPath.row])
        
        return cell
    }
}
extension PatientRegistrationAdultStep8VC: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldPhoneNumber {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
}
extension PatientRegistrationAdultStep8VC: UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "PLEASE EXPLAIN.TYPE DETAILS HERE" || textView.text == "WHAT MEDICATIONS(S) ARE YOU TAKING? TYPE DETAILS HERE" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = textView.tag == 1 ? "PLEASE EXPLAIN.TYPE DETAILS HERE" : "WHAT MEDICATIONS(S) ARE YOU TAKING? TYPE DETAILS HERE"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
extension PatientRegistrationAdultStep8VC: PatientInfoCellDelegate {
    func radioButtonTappedForCell(cell: PatientInfoCell) {
        showPopUp(self.tableView.indexPathForCell(cell)!.row)
    }
    func showPopUp(tag: Int) {
        textViewAnswer.text = tag == 1 ? "PLEASE EXPLAIN.TYPE DETAILS HERE" : "WHAT MEDICATIONS(S) ARE YOU TAKING? TYPE DETAILS HERE"
        textViewAnswer.textColor = UIColor.lightGrayColor()
        textViewAnswer.tag = tag
        self.popUpView.frame = CGRectMake(0, 0, 512.0, 250.0)
        self.popUpView.center = self.view.center
        self.viewShadow.addSubview(self.popUpView)
        self.popUpView.transform = CGAffineTransformMakeScale(0.1, 0.1)
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.popUpView.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
}
