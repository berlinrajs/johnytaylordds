//
//  PatientRegistrationAdultStep10VC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/17/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultStep10VC: PDViewController {
    @IBOutlet weak var viewContainer : UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var radioContactlenses: RadioButton!
    @IBOutlet weak var radioPersistentCough: RadioButton!
    @IBOutlet weak var radiobuttonWoman: RadioButton!
    @IBOutlet weak var radioPregnant: RadioButton!
    @IBOutlet weak var radioNursing: RadioButton!
    @IBOutlet weak var radioOralC: RadioButton!
    
    @IBOutlet weak var popUpView: PDView!
    @IBOutlet weak var textViewAnswer: PDTextView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var buttonAnswered: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if self.patient.gender != 2 {
//            self.radioPregnant.deselectAllButtons()
//            self.radioNursing.deselectAllButtons()
//            self.radioOralC.deselectAllButtons()
//        }

//        if self.patient.patientInformation.isPregnent != nil {
//            self.radioPregnant.setSelectedWithTag(patient.patientInformation.isPregnent!)
//        }
//        if self.patient.patientInformation.isNursing != nil {
//            self.radioNursing.setSelectedWithTag(patient.patientInformation.isNursing!)
//        }
//        if self.patient.patientInformation.oralContraceptives != nil {
//            self.radioOralC.setSelectedWithTag(patient.patientInformation.oralContraceptives!)
//        }
//        
//        if self.patient.patientInformation.contactLense != nil {
//            self.radioContactlenses.setSelectedWithTag(patient.patientInformation.contactLense!)
//        }
//        if self.patient.patientInformation.persistentCough != nil {
//            self.radioPersistentCough.setSelectedWithTag(patient.patientInformation.persistentCough!)
//        }
        // Do any additional setup after loading the view.
        loadValues()
    }
    func loadValues() {
        self.radioContactlenses.setSelectedWithTag(self.patient.patientInformation.contactLense == nil ? 0 : self.patient.patientInformation.contactLense!)
        self.radioPersistentCough.setSelectedWithTag(self.patient.patientInformation.persistentCough == nil ? 0 : self.patient.patientInformation.persistentCough!)
        
        //            if(self.patient.gender == 2) {
        self.radioPregnant.setSelectedWithTag(self.patient.patientInformation.isPregnent == nil ? 0 : self.patient.patientInformation.isPregnent!)
        self.radioNursing.setSelectedWithTag(self.patient.patientInformation.isNursing == nil ? 0 : self.patient.patientInformation.isNursing!)
        self.radioOralC.setSelectedWithTag(self.patient.patientInformation.oralContraceptives == nil ? 0 : self.patient.patientInformation.oralContraceptives!)
        self.radiobuttonWoman.setSelectedWithTag(self.patient.patientInformation.isWomen == nil ? 0 : self.patient.patientInformation.isWomen!)
        self.areUWomanAction(radiobuttonWoman)
    }
    
    func saveValues() {
        self.patient.patientInformation.contactLense = self.radioContactlenses.selectedButton == nil ? 0 : self.radioContactlenses.selectedButton.tag
        self.patient.patientInformation.persistentCough = self.radioPersistentCough.selectedButton == nil ? 0 : self.radioPersistentCough.selectedButton.tag
        
        //            if(self.patient.gender == 2) {
        self.patient.patientInformation.isPregnent = self.radioPregnant.selectedButton == nil ? 0 : self.radioPregnant.selectedButton.tag
        self.patient.patientInformation.isNursing = self.radioNursing.selectedButton == nil ? 0 : self.radioNursing.selectedButton.tag
        self.patient.patientInformation.oralContraceptives = self.radioOralC.selectedButton == nil ? 0 : self.radioOralC.selectedButton.tag
        self.patient.patientInformation.isWomen = self.radiobuttonWoman.selectedButton == nil ? 0 : radiobuttonWoman.selectedButton.tag
    }
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func areUWomanAction (sender : UIButton){
        if radiobuttonWoman.selected == false {
            viewContainer.userInteractionEnabled = false
            viewContainer.alpha = 0.5
            radioPregnant.setSelectedWithTag(2)
            radioNursing.setSelectedWithTag(2)
            radioOralC.setSelectedWithTag(2)
        }else{
            viewContainer.userInteractionEnabled = true
            viewContainer.alpha = 1.0
           
        }
    }

    @IBAction func buttonActionAnswered(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if buttonAnswered.selected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if self.anyUnAnsweredQuestion {
            let alert = Extention.alert("PLEASE ANSWER ALL THE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            saveValues()
            print(patient.patientInformation)
            let newPatientStep11VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep11VCRef") as! PatientRegistrationAdultStep11VC
            newPatientStep11VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep11VC, animated: true)
        }
    }
    var anyUnAnsweredQuestion: Bool {
        get {
            if self.patient.gender.index == 2 {
                if radioPregnant.selectedButton == nil || radioNursing.selectedButton == nil || radioOralC.selectedButton == nil {
                    return true
                }
            }
            if radioContactlenses.selectedButton == nil || radioPersistentCough.selectedButton == nil {
                return true
            }
            for question in self.patient.patientInformation.questionGroup3 {
                if(question.selectedOption == nil) {
                    return true
                }
            }
            return false
        }
    }
}
extension PatientRegistrationAdultStep10VC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    //    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    //        return 40
    //    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.patientInformation.questionGroup3.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PatientInfoCell1", forIndexPath: indexPath) as! PatientInfoCell
        
        cell.delegate = self
        cell.configCell(self.patient.patientInformation.questionGroup3[indexPath.row])
        
        return cell
    }
}
extension PatientRegistrationAdultStep10VC: PatientInfoCellDelegate {
    func radioButtonTappedForCell(cell: PatientInfoCell) {
        showPopUp(self.tableView.indexPathForCell(cell)!.row)
    }
    
    func showPopUp(tag: Int) {
        textViewAnswer.text = "TYPE DETAILS HERE"
        textViewAnswer.textColor = UIColor.lightGrayColor()
        textViewAnswer.tag = tag
        self.popUpView.frame = CGRectMake(0, 0, 512.0, 250.0)
        self.popUpView.center = self.view.center
        self.viewShadow.addSubview(self.popUpView)
        self.popUpView.transform = CGAffineTransformMakeScale(0.1, 0.1)
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.popUpView.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    @IBAction func popupDonePressed(sender: AnyObject) {
        textViewAnswer.resignFirstResponder()
        
        self.popUpView.removeFromSuperview()
        self.viewShadow.hidden = true
        
        if textViewAnswer.isEmpty || textViewAnswer.text == "TYPE DETAILS HERE" {
            self.patient.patientInformation.questionGroup3[textViewAnswer.tag].selectedOption = false
            self.tableView.reloadData()
        } else {
            self.patient.patientInformation.questionGroup3[textViewAnswer.tag].answer = textViewAnswer.text!
        }
    }
}
extension PatientRegistrationAdultStep10VC: UITextViewDelegate {
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "TYPE DETAILS HERE" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = "TYPE DETAILS HERE"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
