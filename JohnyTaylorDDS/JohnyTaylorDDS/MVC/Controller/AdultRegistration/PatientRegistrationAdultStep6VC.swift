//
//  PatientRegistrationAdultStep6VC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/16/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultStep6VC: PDViewController {

    var isSecondaryInsurance: Bool = false
    @IBOutlet var datePickerBirth: UIDatePicker!
    @IBOutlet var datePickerJoin: UIDatePicker!
    
    @IBOutlet var toolBirth: UIToolbar!
    @IBOutlet var toolJoin: UIToolbar!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textFieldName: PDTextField!
    @IBOutlet weak var textFieldRelation: PDTextField!
    @IBOutlet weak var textFieldBirthDate: PDTextField!
    @IBOutlet weak var textFieldSocialSecurity: PDTextField!
//    @IBOutlet weak var textFieldDateEmployed: PDTextField!
    @IBOutlet weak var textFieldEmployerName: PDTextField!
    @IBOutlet weak var textFieldUnion: PDTextField!
    @IBOutlet weak var textFieldWorkPhone: PDTextField!
    @IBOutlet var radiobuttoInsuredSelf: RadioButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.datePickerJoin.maximumDate = NSDate()
        self.datePickerBirth.maximumDate = NSDate()
        
        
        let dateString = "1 Jan 1980"
        let df = NSDateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.dateFromString(dateString)
        if let unwrappedDate = date {
            self.datePickerBirth.setDate(unwrappedDate, animated: false)
        }
        
        self.textFieldBirthDate.inputView = self.datePickerBirth
        self.textFieldBirthDate.inputAccessoryView = self.toolBirth
    
        
        loadValues()
    }
    
    func loadValues() {
        if isSecondaryInsurance{
            labelTitle.text = "SECONDARY INSURANCE INFORMATION"
            if self.patient.secondaryInsurance == nil {
                self.patient.secondaryInsurance = PDInsurance()
            }

        }else{
            labelTitle.text = "PRIMARY INSURANCE INFORMATION"
            if self.patient.primaryInsurance == nil {
                self.patient.primaryInsurance = PDInsurance()
            }

        }
        
        let insuranceSelected : PDInsurance = self.isSecondaryInsurance ? patient.secondaryInsurance : patient.primaryInsurance
        
        radiobuttoInsuredSelf.setSelectedWithTag(insuranceSelected.insSelfButton == nil ? 0 : insuranceSelected.insSelfButton!)
        textFieldName.text = insuranceSelected.name
        textFieldRelation.text = insuranceSelected.relationShipToPatient
        textFieldBirthDate.text = insuranceSelected.birthDate
        
        textFieldEmployerName.text = insuranceSelected.nameOfEmployer
        textFieldUnion.text = insuranceSelected.union
        textFieldWorkPhone.text = insuranceSelected.workPhone
    }
    
    func saveValues() {
      //  self.patient.primaryInsurance = PDInsurance()
        let insuranceSelected : PDInsurance = self.isSecondaryInsurance ? patient.secondaryInsurance : patient.primaryInsurance

        insuranceSelected.insSelfButton = radiobuttoInsuredSelf.selectedButton == nil ? 0 :radiobuttoInsuredSelf.selectedButton.tag
        insuranceSelected.name = textFieldName.text!
        insuranceSelected.relationShipToPatient = textFieldRelation.text!
        insuranceSelected.birthDate = textFieldBirthDate.text!
        
        insuranceSelected.nameOfEmployer = textFieldEmployerName.text!
       insuranceSelected.union = textFieldUnion.text!
        insuranceSelected.workPhone = textFieldWorkPhone.text!
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func radioButtonAction(sender: RadioButton) {
        if radiobuttoInsuredSelf.selectedButton != nil && radiobuttoInsuredSelf.selectedButton.tag == 1 {
            textFieldName.text = patient.fullName
            textFieldBirthDate.text = patient.dateOfBirth
            textFieldUnion.text = patient.phoneNumber
            textFieldEmployerName.text = patient.employerName
            textFieldWorkPhone.text = patient.employerPhone
            textFieldRelation.text = "SELF"
        } else {
            textFieldName.text = patient.responsibleparty.personName
            textFieldBirthDate.text = patient.responsibleparty.birthDate
            textFieldEmployerName.text = patient.responsibleparty.employer
            textFieldUnion.text = patient.responsibleparty.homePhone
            textFieldWorkPhone.text = patient.responsibleparty.workPhone
            textFieldRelation.text = patient.responsibleparty.reloationShipToPatient
        }
    }
    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if textFieldName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER THE INSURANCE HOLDER NAME")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if textFieldBirthDate.isEmpty {
            let alert = Extention.alert("PLEASE ENTER THE INSURANCE HOLDER'S DATE OF BIRTH")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if textFieldEmployerName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER THE NAME OF EMPLOYER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldWorkPhone.isEmpty && !textFieldWorkPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER A VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldUnion.isEmpty && !textFieldUnion.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER A VALID INSURANCE PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let newPatientStep7VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep7VCRef") as! PatientRegistrationAdultStep7VC
            newPatientStep7VC.isSecondaryInsurance = self.isSecondaryInsurance
            newPatientStep7VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep7VC, animated: true)
        }
    }
    
    @IBAction func toolbarDoneButtonActionForBirth(sender: AnyObject) {
        textFieldBirthDate.resignFirstResponder()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldBirthDate.text = dateFormatter.stringFromDate(datePickerBirth.date).uppercaseString
    }
    
    @IBAction func datePickerDateChangedForBirth(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldBirthDate.text = dateFormatter.stringFromDate(datePickerBirth.date).uppercaseString
    }
    
    @IBAction func toolbarDoneButtonActionForJoin(sender: AnyObject) {
//        textFieldDateEmployed.resignFirstResponder()
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "MMMM dd, yyyy"
//        textFieldDateEmployed.text = dateFormatter.stringFromDate(datePickerJoin.date).uppercaseString
    }
    
    @IBAction func datePickerDateChangedForJoin(sender: AnyObject) {
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "MMMM dd, yyyy"
//        textFieldDateEmployed.text = dateFormatter.stringFromDate(datePickerJoin.date).uppercaseString
    }
}
extension PatientRegistrationAdultStep6VC : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldWorkPhone || textField == textFieldUnion {
            return textField.formatPhoneNumber(range, string: string)
            //        } else if textField == textFieldSocialSecurity {
            //            return textField.formatNumbers(range, string: string, count: 9)
        } else if textField == textFieldUnion {
            return string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last == nil
        }
        return true
    }
}
