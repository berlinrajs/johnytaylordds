//
//  PatientRegistrationAdult4VC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/16/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultStep4VC: PDViewController {

    
    @IBOutlet weak var textFieldName: PDTextField!
    @IBOutlet weak var textFieldRelationShip: PDTextField!
    @IBOutlet weak var textFieldAddress: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldZip: PDTextField!
    @IBOutlet weak var textFieldEmail: PDTextField!
    @IBOutlet weak var textFieldHomePhone: PDTextField!
    @IBOutlet weak var textFieldCellPhone: PDTextField!
    
    @IBOutlet var radioButtonSelf: RadioButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        StateListView.addStateListForTextField(textFieldState)
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    func loadValues() {
        if patient.responsibleparty == nil {
            patient.responsibleparty = Person()
        }
        textFieldName.text = patient.responsibleparty.personName
        textFieldRelationShip.text = patient.responsibleparty.reloationShipToPatient
        textFieldAddress.text = patient.responsibleparty.address
        textFieldCity.text = patient.responsibleparty.city
        textFieldState.text = patient.responsibleparty.state == nil ? "TX" : patient.responsibleparty.state
        textFieldZip.text = patient.responsibleparty.zip
        textFieldEmail.text = patient.responsibleparty.email
        textFieldHomePhone.text = patient.responsibleparty.homePhone
        textFieldCellPhone.text = patient.responsibleparty.cellPhone
        radioButtonSelf.setSelectedWithTag(patient.responsibleparty.resSelfButton == nil ? 0 : patient.responsibleparty.resSelfButton)
    }
    
    func saveValues() {
        
        patient.responsibleparty.personName = textFieldName.text!
        patient.responsibleparty.reloationShipToPatient = textFieldRelationShip.text!
        patient.responsibleparty.address = textFieldAddress.text!
        patient.responsibleparty.city = textFieldCity.text!
        patient.responsibleparty.state = textFieldState.text!
        patient.responsibleparty.zip = textFieldZip.text!
        patient.responsibleparty.email = textFieldEmail.text!
        patient.responsibleparty.homePhone = textFieldHomePhone.text!
        patient.responsibleparty.cellPhone = textFieldCellPhone.text!
        patient.responsibleparty.resSelfButton = radioButtonSelf.selectedButton == nil ? 0 : radioButtonSelf.selectedButton.tag
    }
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func radioButtonSelfAction(sender: RadioButton) {
        if sender.tag == 1 {
            textFieldName.text = patient.fullName  == "N/A" ? "" : patient.fullName
            textFieldAddress.text = patient.addressLine  == "N/A" ? "" : patient.addressLine
            textFieldState.text = patient.state  == "N/A" ? "" : patient.state
            textFieldCity.text = patient.city == "N/A" ? "" : patient.city
            textFieldZip.text = patient.zipCode == "N/A" ? "" : patient.zipCode
            textFieldEmail.text = patient.email == "N/A" ? "" : patient.email
            textFieldHomePhone.text = patient.phoneNumber == "N/A" ? "" : patient.phoneNumber
            textFieldCellPhone.text = patient.cellPhoneNumber == "N/A" ? "" : patient.cellPhoneNumber
            textFieldRelationShip.text = "SELF"
            
        } else {
            
            textFieldName.text = ""
            textFieldAddress.text = ""
            textFieldState.text = "TX"
            textFieldCity.text = ""
            textFieldZip.text = ""
            textFieldEmail.text = ""
            textFieldHomePhone.text = ""
            textFieldCellPhone.text = ""
            
        }
        
        
    }
    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        
        if !textFieldZip.isEmpty && !textFieldZip.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIP CODE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if (!textFieldCellPhone.isEmpty && !textFieldCellPhone.text!.isPhoneNumber) || (!textFieldHomePhone.isEmpty && !textFieldHomePhone.text!.isPhoneNumber) {
            let alert = Extention.alert("PLEASE ENTER THE VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail {
            let alert = Extention.alert("PLEASE ENTER A VALID EMAIL ID")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let newPatientStep5VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep5VCRef") as! PatientRegistrationAdultStep5VC
            newPatientStep5VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep5VC, animated: true)
        }
    }
}
extension PatientRegistrationAdultStep4VC : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldHomePhone || textField == textFieldCellPhone {
            return textField.formatPhoneNumber(range, string: string)
        } else if textField == textFieldZip {
            return textField.formatZipCode(range, string: string)
        } 
        return true
    }
}
