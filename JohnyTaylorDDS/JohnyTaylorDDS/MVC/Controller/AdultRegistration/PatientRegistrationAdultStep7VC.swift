//
//  PatientRegistrationAdultStep7VC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/16/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultStep7VC: PDViewController {
    
    var isSecondaryInsurance: Bool = false
    
    @IBOutlet weak var textFieldEmployerAddress: PDTextField!
    @IBOutlet weak var textFieldEmployerCity: PDTextField!
    @IBOutlet weak var textFieldEmployerState: PDTextField!
    @IBOutlet weak var textFieldEmployerZip: PDTextField!
    @IBOutlet weak var textFieldInsuranceCompany: PDTextField!
    @IBOutlet weak var textFieldGroup: PDTextField!
    @IBOutlet weak var textFieldPolicyId: PDTextField!
    @IBOutlet weak var textFieldCompanyAddress: PDTextField!
    @IBOutlet weak var textFieldCompanyState: PDTextField!
    @IBOutlet weak var textFieldCompanyCity: PDTextField!
    @IBOutlet weak var textFieldCompanyZip: PDTextField!
//    @IBOutlet weak var textFieldDeductible: PDTextField!
//    @IBOutlet weak var textFieldUsedAmount: PDTextField!
//    @IBOutlet weak var textFieldMaxBenefit: PDTextField!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewPopUp: PDView!
    @IBOutlet weak var viewShadow: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        if isSecondaryInsurance {
        //            labelTitle.text = "SECONDARY INSURANCE INFORMATION"
        //        }
        
        StateListView.addStateListForTextField(textFieldEmployerState)
        StateListView.addStateListForTextField(textFieldCompanyState)
        
        loadValues()
        
        if patient.primaryInsurance.insSelfButton == 1 {
            textFieldEmployerAddress.text = patient.addressLine
            textFieldEmployerState.text = patient.state
            textFieldEmployerCity.text = patient.city
            textFieldEmployerZip.text = patient.zipCode
            
        } else {
            textFieldEmployerAddress.text = patient.responsibleparty.address == "N/A" ? "" :patient.responsibleparty.address
            textFieldEmployerState.text = patient.responsibleparty.state
            textFieldEmployerCity.text = patient.responsibleparty.city == "N/A" ? "" : patient.responsibleparty.city
            textFieldEmployerZip.text = patient.responsibleparty.zip == "N/A" ? "" : patient.responsibleparty.zip
        }
    }
    
    func loadValues() {
        if isSecondaryInsurance{
            labelTitle.text = "SECONDARY INSURANCE INFORMATION"
        }else{
            labelTitle.text = "PRIMARY INSURANCE INFORMATION"
        }
        let insuranceSelected : PDInsurance = self.isSecondaryInsurance ? patient.secondaryInsurance : patient.primaryInsurance

        textFieldEmployerAddress.text = insuranceSelected.address
        textFieldEmployerCity.text = insuranceSelected.city
        textFieldEmployerState.text = insuranceSelected.state == "" ? "TX" : insuranceSelected.state
        textFieldEmployerZip.text = insuranceSelected.zip
        textFieldInsuranceCompany.text = insuranceSelected.insuranceCompName
        textFieldGroup.text = insuranceSelected.insuranceGroup
        textFieldPolicyId.text = insuranceSelected.insurancePolicyID
        textFieldCompanyAddress.text = insuranceSelected.insuranceCompanyAddress
        textFieldCompanyState.text = insuranceSelected.insuranceCompanyState == "" ? "TX" : insuranceSelected.insuranceCompanyState
        textFieldCompanyCity.text = insuranceSelected.insuranceCompanyCity
        textFieldCompanyZip.text = insuranceSelected.insuranceCompanyZip
    }
    
    func saveValues() {
        let insuranceSelected : PDInsurance = self.isSecondaryInsurance ? patient.secondaryInsurance : patient.primaryInsurance

        insuranceSelected.address = textFieldEmployerAddress.text!
        insuranceSelected.city = textFieldEmployerCity.text!
        insuranceSelected.state = textFieldEmployerState.text!
        insuranceSelected.zip = textFieldEmployerZip.text!
        insuranceSelected.insuranceCompName = textFieldInsuranceCompany.text!
        insuranceSelected.insuranceGroup = textFieldGroup.text!
        insuranceSelected.insurancePolicyID = textFieldPolicyId.text!
        insuranceSelected.insuranceCompanyAddress = textFieldCompanyAddress.text!
        insuranceSelected.insuranceCompanyState = textFieldCompanyState.text!
        insuranceSelected.insuranceCompanyCity = textFieldCompanyCity.text!
        insuranceSelected.insuranceCompanyZip = textFieldCompanyZip.text!
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if (!textFieldEmployerZip.isEmpty && !textFieldEmployerZip.text!.isZipCode) || (!textFieldCompanyZip.isEmpty && !textFieldCompanyZip.text!.isZipCode) {
            let alert = Extention.alert("PLEASE ENTER A VALID ZIP CODE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            saveValues()
            if isSecondaryInsurance{
                patient.haveSecondaryInsurance = 1
                let newPatientStep8VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep8VCRef") as! PatientRegistrationAdultStep8VC
                newPatientStep8VC.patient = self.patient
                self.navigationController?.pushViewController(newPatientStep8VC, animated: true)

            }else{
            YesOrNoAlert.sharedInstance.showWithTitle("DO YOU HAVE SECONDARY INSURANCE", button1Title: "YES", button2Title: "NO", completion: { (buttonIndex) in
                if buttonIndex == 1{
                    let newPatientStep6VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep6VCRef") as! PatientRegistrationAdultStep6VC
                    newPatientStep6VC.patient = self.patient
                    newPatientStep6VC.isSecondaryInsurance = true
                    self.navigationController?.pushViewController(newPatientStep6VC, animated: true)

                }else{
                    let newPatientStep8VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep8VCRef") as! PatientRegistrationAdultStep8VC
                    newPatientStep8VC.patient = self.patient
                    self.navigationController?.pushViewController(newPatientStep8VC, animated: true)
 
                }
            })
            }
        }
    }
    
    //    @IBAction func secondaryInsuranceYesAction(sender: AnyObject) {
    //        self.viewPopUp.removeFromSuperview()
    //        self.viewShadow.hidden = true
    //        self.patient.haveSecondaryInsurance = 1
    //
    //        let newPatientStep6VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep6VCRef") as! PatientRegistrationAdultStep6VC
    //        newPatientStep6VC.patient = self.patient
    //        newPatientStep6VC.isSecondaryInsurance = true
    //        self.navigationController?.pushViewController(newPatientStep6VC, animated: true)
    //    }
    
    //    @IBAction func secondaryInsuranceNoAction(sender: AnyObject) {
    //        self.viewPopUp.removeFromSuperview()
    //        self.viewShadow.hidden = true
    //        self.patient.haveSecondaryInsurance = 0
    //
    //        let newPatientStep8VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep8VCRef") as! PatientRegistrationAdultStep8VC
    //        newPatientStep8VC.patient = self.patient
    //        self.navigationController?.pushViewController(newPatientStep8VC, animated: true)
    //    }
    
    func showPopup() {
        self.viewPopUp.frame = CGRectMake(0, 0, 512.0, 250.0)
        self.viewPopUp.center = self.view.center
        self.viewShadow.addSubview(self.viewPopUp)
        self.viewPopUp.transform = CGAffineTransformMakeScale(0.1, 0.1)
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewPopUp.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
}
extension PatientRegistrationAdultStep7VC : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldEmployerZip || textField == textFieldCompanyZip {
            return textField.formatZipCode(range, string: string)
        } else if textField == textFieldPolicyId {
            return textField.formatNumbers(range, string: string, count: 20) //string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last == nil
//        } else if textField == textFieldDeductible || textField == textFieldMaxBenefit || textField == textFieldUsedAmount {
//            return string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890.").invertedSet)?.last == nil
            //        } else if textField == textFieldGroup {
            //            return string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last == nil
        }
        return true
    }
}
