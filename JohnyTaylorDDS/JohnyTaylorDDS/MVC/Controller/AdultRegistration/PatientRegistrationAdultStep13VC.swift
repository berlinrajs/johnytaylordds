//
//  PatientRegistrationAdultStep13VC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/17/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultStep13VC: PDViewController {

    @IBOutlet var textViewComments: PDTextView!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelSignature: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelSignature.text = self.patient.maritialStatus.index == 6 ? "Signature of the Patient's Parent/Guardian" : "Signature of the Patient"
        labelName.text = self.patient.maritialStatus.index == 6 ? "" : patient.fullName
            
        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    func labelTapped(sender: AnyObject) {
        labelDate.text = self.patient.dateToday
        labelDate.textColor = UIColor.blackColor()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            self.patient.patientInformation.patientOrGuardianSignature = self.signatureView.signatureImage()
            
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultFormVCRef") as! PatientRegistrationAdultFormVC
            patient.textOthers = textViewComments.text == "TYPE HERE" ? "" : textViewComments.text
            formVC.patient = self.patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
    extension PatientRegistrationAdultStep13VC : UITextViewDelegate {
        
        func textViewDidBeginEditing(textView: UITextView) {
            if textView.text == "TYPE HERE" || textView.text == "" {
                textView.text = ""
                textView.textColor = UIColor.blackColor()
            }
        }
        
        func textViewDidEndEditing(textView: UITextView) {
            if textView.text.isEmpty {
                textView.textColor = UIColor.lightGrayColor()
            }
        }
        func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
            if text == "\n" {
                textView.resignFirstResponder()
            }
            return true
        }
    }

   
