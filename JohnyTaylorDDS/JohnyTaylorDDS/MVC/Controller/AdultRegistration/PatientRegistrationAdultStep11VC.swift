//
//  PatientRegistrationAdultStep11VC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/17/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultStep11VC: PDViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textFieldDentistName: PDTextField!
    @IBOutlet weak var textFieldLastDate: PDTextField!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var toolBar: UIToolbar!
    
    @IBOutlet weak var radioClicking: RadioButton!
    @IBOutlet weak var radioPain: RadioButton!
    @IBOutlet weak var radioOpenOrClose: RadioButton!
    @IBOutlet weak var radioChewing: RadioButton!
    @IBOutlet weak var buttonAnswered: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        datePicker.maximumDate = NSDate()
        textFieldLastDate.inputView = datePicker
        textFieldLastDate.inputAccessoryView = toolBar
        let dateString = "1 Jan \(NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: NSDate()))"
        let df = NSDateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.dateFromString(dateString)
        if let unwrappedDate = date {
            self.datePicker.setDate(unwrappedDate, animated: false)
        }
        
        loadValues()
    }
    func loadValues() {
        if self.patient.dentalInformation == nil {
            self.patient.dentalInformation = DentalInformation.adultDentalInformation()
        }
        
        self.textFieldDentistName.text = self.patient.dentalInformation.previousDentistNameAndLocation
        self.textFieldLastDate.text = self.patient.dentalInformation.dateOfLastExam
        
        self.radioClicking.setSelectedWithTag(self.patient.dentalInformation.clicking == nil ? 0 : self.patient.dentalInformation.clicking!)
        self.radioPain.setSelectedWithTag(self.patient.dentalInformation.pain == nil ? 0 : self.patient.dentalInformation.pain!)
        self.radioOpenOrClose.setSelectedWithTag(self.patient.dentalInformation.openOrClose == nil ? 0 : self.patient.dentalInformation.openOrClose!)
        self.radioChewing.setSelectedWithTag(self.patient.dentalInformation.chewing == nil ? 0 : self.patient.dentalInformation.chewing!)
        
        self.tableView.reloadData()
    }
    
    func saveValues() {
        self.patient.dentalInformation.previousDentistNameAndLocation = self.textFieldDentistName.text
        self.patient.dentalInformation.dateOfLastExam = self.textFieldLastDate.text!
        
        self.patient.dentalInformation.clicking = self.radioClicking.selectedButton == nil ? 0 : self.radioClicking.selectedButton.tag
        self.patient.dentalInformation.pain = self.radioPain.selectedButton == nil ? 0 : self.radioPain.selectedButton.tag
        self.patient.dentalInformation.openOrClose = self.radioOpenOrClose.selectedButton == nil ? 0 : self.radioOpenOrClose.selectedButton.tag
        self.patient.dentalInformation.chewing = self.radioChewing.selectedButton == nil ? 0 : self.radioChewing.selectedButton.tag
    }
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldLastDate.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    
    @IBAction func datePickerDonePressed(sender: AnyObject) {
        textFieldLastDate.resignFirstResponder()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldLastDate.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    
    @IBAction func buttonActionAnswered(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if buttonAnswered.selected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if self.anyUnAnsweredQuestion {
            let alert = Extention.alert("PLEASE ANSWER ALL THE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let newPatientStep12VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep12VCRef") as! PatientRegistrationAdultStep12VC
            newPatientStep12VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep12VC, animated: true)
        }
    }
    var anyUnAnsweredQuestion: Bool {
        get {
            if radioClicking.selectedButton == nil || radioPain.selectedButton == nil || radioChewing.selectedButton == nil || radioOpenOrClose.selectedButton == nil {
                return true
            }
            for question in self.patient.dentalInformation.questionGroup1 {
                if question.selectedOption == nil {
                    return true
                }
            }
            return false
        }
    }
}
extension PatientRegistrationAdultStep11VC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.dentalInformation.questionGroup1.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PatientInfoCell1", forIndexPath: indexPath) as! PatientInfoCell
        
        cell.configCellWithAsterix(self.patient.dentalInformation.questionGroup1[indexPath.row])
        
        return cell
    }
}
