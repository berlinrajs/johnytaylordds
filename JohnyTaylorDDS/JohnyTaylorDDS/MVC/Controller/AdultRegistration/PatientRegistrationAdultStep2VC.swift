//
//  PatientRegistrationAdultStep2VC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/16/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultStep2VC: PDViewController {

    
    @IBOutlet weak var textFieldEmployerName: PDTextField!
    @IBOutlet weak var textFieldBusinessAddress: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldZipCode: PDTextField!
    @IBOutlet weak var textFieldWorkPhone: PDTextField!
    @IBOutlet weak var textFieldSpouseName: PDTextField!
    @IBOutlet weak var textFieldSpouseEmployer: PDTextField!
    @IBOutlet weak var textFieldSpouseWorkPhone: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StateListView.addStateListForTextField(textFieldState)
        
        self.textFieldSpouseName.enabled = patient.maritialStatus.index != 1 && patient.maritialStatus.index != 6
        self.textFieldSpouseEmployer.enabled = patient.maritialStatus.index != 1 && patient.maritialStatus.index != 6
        self.textFieldSpouseWorkPhone.enabled = patient.maritialStatus.index != 1 && patient.maritialStatus.index != 6
        // Do any additional setup after loading the view.
        
        loadValues()
        #if AUTO
            if let patientDetails = patient.patientDetails where patient.employerPhone == nil {
                textFieldWorkPhone.text = patientDetails.workPhone
            }
        #endif
        
    }
    
    func loadValues() {
        textFieldEmployerName.text = patient.employerName
        textFieldBusinessAddress.text = patient.employerAddress
        textFieldWorkPhone.text = patient.employerPhone
        textFieldCity.text = patient.employerCity
        textFieldState.text = patient.employerState == nil ? "TX" : patient.employerState
        textFieldZipCode.text = patient.employerZip
        
        textFieldSpouseName.text = patient.spouseName
        textFieldEmployerName.text = patient.spouseEmployerName
        textFieldSpouseWorkPhone.text = patient.spouseWorkPhone
    }
    
    func saveValues() {
        patient.employerName = textFieldEmployerName.text!
        patient.employerAddress = textFieldBusinessAddress.text!
        patient.employerPhone = textFieldWorkPhone.text!
        patient.employerCity = textFieldCity.text!
        patient.employerState = textFieldState.text
        patient.employerZip = textFieldZipCode.text
        
        patient.spouseName = textFieldSpouseName.text
        patient.spouseEmployerName = textFieldEmployerName.text
        patient.spouseWorkPhone = textFieldSpouseWorkPhone.text
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if !textFieldZipCode.isEmpty && !textFieldZipCode.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIPCODE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldWorkPhone.isEmpty && !textFieldWorkPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldSpouseWorkPhone.isEmpty && !textFieldSpouseWorkPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let newPatientStep3VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep3VCRef") as! PatientRegistrationAdultStep3VC
            newPatientStep3VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep3VC, animated: true)
        }
    }
}

extension PatientRegistrationAdultStep2VC : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        } else if textField == textFieldWorkPhone {
            return textField.formatPhoneNumber(range, string: string)
        } else if textField == textFieldSpouseWorkPhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
}
