//
//  PatientRegistrationAdultStep3VC.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/16/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationAdultStep3VC: PDViewController {

    
    @IBOutlet weak var radioButtonStudent : RadioButton!
    @IBOutlet weak var textFieldSchoolName: PDTextField!
    @IBOutlet weak var textFieldSchoolState: PDTextField!
    @IBOutlet weak var textFieldSchoolCity: PDTextField!
    @IBOutlet weak var radioButtonStudyType: RadioButton!
    @IBOutlet weak var radioButtonStudyTypePartTime: RadioButton!
    @IBOutlet weak var textFieldReference: PDTextField!
    @IBOutlet weak var textFieldemergencyName: PDTextField!
    @IBOutlet weak var textFieldEmergencyPhone: PDTextField!
    
    @IBOutlet weak var viewStudent: PDView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.patient.isStudent == nil || self.patient.isStudent == 2 {
            self.radioButtonStudent.setSelectedWithTag(2)
            self.setInactive()
        } else {
            self.radioButtonStudent.setSelectedWithTag(1)
            self.setActive()
        }
        
        StateListView.addStateListForTextField(textFieldSchoolState)

        // Do any additional setup after loading the view.
        loadValues()
    }
    
    func loadValues() {
        self.radioButtonStudent.setSelectedWithTag(patient.isStudent == nil ? 100 : patient.isStudent!)
        self.textFieldSchoolName.text = patient.schoolName
        self.textFieldSchoolCity.text = patient.schoolCity
        self.textFieldSchoolState.text = patient.schoolState == nil ? "" : patient.schoolState
        self.radioButtonStudyType.setSelectedWithTag(patient.studyType == nil ? 100 : patient.studyType!)
        self.textFieldemergencyName.text = patient.emergencyContactName
        self.textFieldEmergencyPhone.text = patient.emergencyContactPhoneNumber
        textFieldReference.text = patient.referredBy
    }
    
    func saveValues() {
        patient.isStudent = self.radioButtonStudent.selectedButton == nil ? 0 : self.radioButtonStudent.selectedButton.tag
        patient.schoolName = self.textFieldSchoolName.text!
        patient.schoolCity = self.textFieldSchoolCity.text!
        patient.schoolState = self.textFieldSchoolState.text!
        patient.studyType = self.radioButtonStudyType.selectedButton == nil ? 0 : self.radioButtonStudyType.selectedButton.tag
        patient.emergencyContactName = self.textFieldemergencyName.text!
        patient.emergencyContactPhoneNumber = self.textFieldEmergencyPhone.text!
        patient.referredBy = textFieldReference.text!
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }
    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if textFieldemergencyName.isEmpty || textFieldEmergencyPhone.isEmpty{
            let alert = Extention.alert("PLEASE ENTER EMERGECY CONTACT DETAILS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldEmergencyPhone.isEmpty && !textFieldEmergencyPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID EMERGENCY CONTACT NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let newPatientStep4VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationAdultStep4VCRef") as! PatientRegistrationAdultStep4VC
            newPatientStep4VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep4VC, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func radioButtonStudentAction(sender: RadioButton) {
        if(sender == self.radioButtonStudent) {
            self.setActive()
        } else {
            self.setInactive()
        }
    }
    
    func setInactive() {
        viewStudent.layer.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.3).CGColor
        viewStudent.userInteractionEnabled = false
        
        textFieldSchoolName.enabled = false
        textFieldSchoolCity.enabled = false
        textFieldSchoolState.enabled = false
        radioButtonStudyType.enabled = false
        radioButtonStudyType.alpha = 0.5
        
        textFieldSchoolName.text = ""
        textFieldSchoolCity.text = ""
        textFieldSchoolState.text = ""
        radioButtonStudyType.deselectAllButtons()
        
        radioButtonStudyTypePartTime.enabled = false
        radioButtonStudyTypePartTime.alpha = 0.5
    }
    func setActive() {
        
        viewStudent.layer.borderColor = UIColor.whiteColor().CGColor
        viewStudent.userInteractionEnabled = true
        textFieldSchoolName.enabled = true
        textFieldSchoolCity.enabled = true
        textFieldSchoolState.enabled = true
        radioButtonStudyType.enabled = true
        radioButtonStudyType.alpha = 1.0
        
        textFieldSchoolState.text = "TX"
        
        radioButtonStudyTypePartTime.enabled = true
        radioButtonStudyTypePartTime.alpha = 1.0
    }
}
extension PatientRegistrationAdultStep3VC : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldEmergencyPhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
}
