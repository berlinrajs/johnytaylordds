//
//  FeedBackViewController.swift
//  DistinctiveDentalCare
//
//  Created by Berlin Raj on 25/08/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FeedBackViewController: PDViewController {
    
    @IBOutlet weak var textViewComment: PDTextView!
    @IBOutlet weak var buttonAllowMessage: UIButton!
    @IBOutlet weak var buttonAnonymous: UIButton!
    @IBOutlet weak var viewRating: UIView!
    
    var ratingView : HCSStarRatingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ratingView = HCSStarRatingView(frame: viewRating.bounds)
        ratingView.allowsHalfStars = true
        ratingView.emptyStarImage = UIImage(named: "ReviewNoStar")
        ratingView.halfStarImage = UIImage(named: "ReviewHalfStar")
        ratingView.filledStarImage = UIImage(named: "ReviewFullStar")
        ratingView.spacing = 5.0
        ratingView.maximumValue = 5
        ratingView.backgroundColor = UIColor.clearColor()
        viewRating.addSubview(ratingView)
        // Do any additional setup after loading the view.
        
        loadValues()
    }
    
    func loadValues() {
        self.textViewDidBeginEditing(textViewComment)
        textViewComment.text = patient.feedBackComment == nil ? "" : patient.feedBackComment
        self.textViewDidEndEditing(textViewComment)
        
        ratingView.value = patient.rating == nil ? 0.0 : patient.rating
        buttonAnonymous.selected = patient.isAnonymous
        buttonAllowMessage.selected = patient.allowMessage
    }
    
    func saveValues() {
        self.textViewDidBeginEditing(textViewComment)
        patient.feedBackComment = textViewComment.text
        self.textViewDidEndEditing(textViewComment)
        
        patient.rating = ratingView.value
        patient.isAnonymous = buttonAnonymous.selected
        patient.allowMessage = buttonAllowMessage.selected
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        saveValues()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonSubmitAction(sender: UIButton) {
        self.view.endEditing(true)
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "Johny Taylor DDS", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            BRProgressHUD.show()
            ServiceManager.postReview(patient.firstName + " " + patient.lastName, comment: textViewComment.text == "Please share your thoughts. Let us know if there is anything we can improve on." ? "" : textViewComment.text!, rating: ratingView.value, phoneNumber: patient.phoneNumber!, allowMessage: self.buttonAllowMessage.selected, email: "", anonymous: buttonAnonymous.selected, completion: { (success, error) in
                BRProgressHUD.hide()
                if success {
                    self.showAlert(self.ratingView.value >= 4.0 && self.buttonAllowMessage.selected == true ? "Thanks for your feedback, Please check your phone" : "Thanks for your feedback", completion: { (completed) in
                        if completed {
                            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
                            self.navigationController?.popToRootViewControllerAnimated(true)
                        }
                    })
                } else {
                    self.showAlert(error!.localizedDescription)
                }
            })
        }
    }
    @IBAction func buttonAllowAction(sender: UIButton) {
        sender.selected = !sender.selected
    }
}
extension FeedBackViewController: UITextViewDelegate {
    func textViewDidEndEditing(textView: UITextView) {
        if textViewComment.text == "" {
            textViewComment.text = "Please share your thoughts. Let us know if there is anything we can improve on."
            textViewComment.textColor = UIColor.lightGrayColor()
        }
    }
    func textViewDidBeginEditing(textView: UITextView) {
        if textViewComment.text == "" || textViewComment.text == "Please share your thoughts. Let us know if there is anything we can improve on." {
            textViewComment.text = ""
            textViewComment.textColor = UIColor.blackColor()
        }
    }
}
