//
//  MedicareFormViewController.swift
//  JohnyTaylorDDS
//
//  Created by Bala Murugan on 8/12/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicareFormViewController: PDViewController {

    @IBOutlet weak var labelExecutionDate : UILabel!
    @IBOutlet weak var imageviewPatient : UIImageView!
    @IBOutlet weak var imageviewWitness : UIImageView!
    @IBOutlet weak var labelDate1  : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelRelationship : UILabel!
    @IBOutlet weak var labelName : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelExecutionDate.text = patient.selectedForms.first!.toothNumbers
        imageviewPatient.image = patient.medicareSignature
        imageviewWitness.image = patient.medicareWitness
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelRelationship.text = patient.medicareRelation
        labelName.text = patient.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
