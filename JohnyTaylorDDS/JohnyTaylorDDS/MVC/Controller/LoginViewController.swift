//
//  LoginViewController.swift
//  OptimaDentistry
//
//  Created by SRS Web Solutions on 12/07/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class LoginViewController: PDViewController {

    @IBOutlet weak var textFieldUserName: PDTextField!
    @IBOutlet weak var textFieldPassword: PDTextField!
    @IBOutlet weak var labelVersion: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(dateChangedNotification), name: kDateChangedNotification, object: nil)
        self.dateChangedNotification()
        labelVersion.text = NSBundle.mainBundle().objectForInfoDictionaryKey(kCFBundleVersionKey as String) as? String
        // Do any additional setup after loading the view.
    }
    
    func dateChangedNotification() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        labelDate.text = dateFormatter.stringFromDate(NSDate()).uppercaseString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonSubmitAction() {
        if textFieldUserName.isEmpty || !textFieldUserName.text!.isValidEmail {
            let alert = Extention.alert("PLEASE ENTER THE VALID EMAIL")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if textFieldPassword.isEmpty {
            let alert = Extention.alert("PLEASE ENTER THE PASSWORD")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            self.submitAction()
        }
    }
    
    func submitAction() {
        self.view.endEditing(true)
        ServiceManager.loginWithUsername(textFieldUserName.text!, password: textFieldPassword.text!) { (success, error) -> (Void) in
            if success {
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: kAppLoggedInKey)
                NSUserDefaults.standardUserDefaults().setValue(self.textFieldUserName.text!, forKey: kAppLoginUsernameKey)
                NSUserDefaults.standardUserDefaults().setValue(self.textFieldPassword.text!, forKey: kAppLoginPasswordKey)
                NSUserDefaults.standardUserDefaults().synchronize()
                (UIApplication.sharedApplication().delegate as! AppDelegate).checkAutologin()
            } else {
                if error == nil {
                    let alert = Extention.alert("PLEASE CHECK YOUR INTERNET CONNECTION AND TRY AGAIN")
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    let alert = Extention.alert(error!.localizedDescription.uppercaseString)
                    self.presentViewController(alert, animated: true, completion: nil)
//                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: "kApploggedIn")
//                    NSUserDefaults.standardUserDefaults().synchronize()
//                    (UIApplication.sharedApplication().delegate as! AppDelegate).checkAutologin()
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == textFieldUserName {
            textFieldPassword.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            self.submitAction()
        }
        return true
    }
}