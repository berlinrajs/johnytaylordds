//
//  CommunicationFormViewController.swift
//  JohnyTaylorDDS
//
//  Created by Bala Murugan on 8/12/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CommunicationFormViewController: PDViewController {

    @IBOutlet weak var imageviewPatient : UIImageView!
    @IBOutlet weak var imageviewWitness : UIImageView!
    @IBOutlet weak var labelDate1  : UILabel!
    @IBOutlet weak var labelEmail : UILabel!
    @IBOutlet weak var labelCell : UILabel!
    
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var labelName1 : UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()

        imageviewPatient.image = patient.communicationSignature
        imageviewWitness.image = patient.communicationWittness
        labelDate1.text = patient.dateToday
        labelEmail.text = patient.communicationEmail
        labelCell.text = patient.communicationCell
        labelName.text = patient.fullName
        labelName1.text = patient.fullName

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
