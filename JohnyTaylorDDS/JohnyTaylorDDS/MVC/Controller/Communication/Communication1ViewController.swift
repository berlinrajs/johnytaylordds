//
//  Communication1ViewController.swift
//  JohnyTaylorDDS
//
//  Created by Bala Murugan on 8/12/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Communication1ViewController: PDViewController {

    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var signatureWitness : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!
    @IBOutlet weak var textfieldCell : UITextField!
    @IBOutlet weak var textfieldEmail : UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        let str : NSString = labelName.text!.stringByReplacingOccurrencesOfString("KPATIENTNAME", withString: patient.fullName)
        let range = str.rangeOfString(patient.fullName)
        let attributedString = NSMutableAttributedString(string: str as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
        labelName.attributedText = attributedString
        
        loadValues()
    }
    
    func loadValues() {
        textfieldEmail.text = patient.communicationEmail
        textfieldCell.text = patient.communicationCell
    }
    
    func saveValues() {
        
        patient.communicationSignature = signaturePatient.signatureImage()
        patient.communicationWittness = signatureWitness.signatureImage()
        patient.communicationEmail = textfieldEmail.text!
        patient.communicationCell = textfieldCell.text!
    }
    
    override func buttonBackAction(sender: AnyObject) {
        saveValues()
        super.buttonBackAction(sender)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
       if !textfieldCell.isEmpty && !textfieldCell.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID CELL NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)

        }else if !textfieldEmail.isEmpty && !textfieldEmail.text!.isValidEmail{
            let alert = Extention.alert("PLEASE ENTER THE VALID EMAIL")
            self.presentViewController(alert, animated: true, completion: nil)

        }else if !signaturePatient.isSigned() || !signatureWitness.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else{
        saveValues()
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("CommunicationFormVC") as! CommunicationFormViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
    }


    
}

extension Communication1ViewController : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldCell{
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }

    
}

