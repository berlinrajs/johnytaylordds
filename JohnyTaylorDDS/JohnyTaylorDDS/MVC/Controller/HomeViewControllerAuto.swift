//
//  HomeViewController.swift
//  AdultMedicalForm
//
//  Created by SRS Web Solutions on 06/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HomeViewControllerAuto: PDViewController {
    @IBOutlet var lblDate: UILabel!
    
    @IBOutlet weak var tableViewForms: UITableView!
    
    @IBOutlet weak var textFieldToothNumbers: PDTextField!
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet var viewToothNumbers: PDView!
    @IBOutlet weak var viewAlert: PDView!
    @IBOutlet weak var labelVersion: UILabel!
    @IBOutlet weak var labelNetwork: UILabel!

    
    
    @IBOutlet weak var constraintTableViewTop: NSLayoutConstraint!
    @IBOutlet weak var constraintTableViewBottom: NSLayoutConstraint!
    
    var isNewPatient: Bool! = false

    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let text = NSBundle.mainBundle().infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        self.navigationController?.navigationBar .hidden = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(showAlertPopup), name: kFormsCompletedNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(dateChangedNotification), name: kDateChangedNotification, object: nil)

        self.dateChangedNotification()
        
    }
    
    
    func internetStatusChanges() {
        if !Reachability.isConnectedToNetwork(){
            dispatch_async(dispatch_get_main_queue(), {
                self.labelNetwork.text = "Internet : \n OFF"

            })
        }else{
            self.labelNetwork.text = "Internet : \n ON"

        }
        self.performSelector(#selector(HomeViewControllerAuto.internetStatusChanges), withObject: nil, afterDelay: 5.0)
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        NSObject.cancelPreviousPerformRequestsWithTarget(self)
    }
    override func viewWillAppear(animated: Bool) {
        self.internetStatusChanges()
        super.viewWillAppear(animated)
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
            let defaults = NSUserDefaults.standardUserDefaults()
            ServiceManager.loginWithUsername(defaults.valueForKey(kAppLoginUsernameKey) as! String, password: defaults.valueForKey(kAppLoginPasswordKey) as! String) { (success, error) -> (Void) in
                if success {
                    
                } else {
                    if error == nil {
                        
                    } else {
                        dispatch_async(dispatch_get_main_queue(), {
                            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "kApploggedIn")
                            NSUserDefaults.standardUserDefaults().synchronize()
                            (UIApplication.sharedApplication().delegate as! AppDelegate).checkAutologin()
                        })
                    }
                }
            }
        }
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            self.formList = forms
            self.tableViewForms.reloadData()
            if isConnectionFailed == true {
                let alertController = UIAlertController(title: "Johny Taylor DDS", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.sharedApplication().openURL(url)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func dateChangedNotification() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        self.lblDate.text = dateFormatter.stringFromDate(NSDate()).uppercaseString
    }

    
    
    @IBAction func btnNextAction(sender: AnyObject) {
        selectedForms.removeAll()
        for (_, form) in formList.enumerate() {
            if form.isSelected == true {
//                if form.formTitle == kConsentForms  {
                    for subForm in form.subForms {
                        if subForm.isSelected == true {
                            selectedForms.append(subForm)
                        }
                    }
//                } else {
//                    selectedForms.append(form)
//                }
            }
        }
        self.view.endEditing(true)
        
        if selectedForms.count > 0 {
//            selectedForms.sortInPlace({ (formObj1, formObj2) -> Bool in
//                return formObj1.index < formObj2.index
//            })
            self.view.layoutIfNeeded()
            constraintTableViewTop.constant = 100
            UIView.animateWithDuration(0.2, animations: {
                self.view.layoutIfNeeded()
            }) { (finished) in
                self.constraintTableViewBottom.constant = 201
            }
            if selectedForms.count == 1 && selectedForms.first!.formTitle == kFeedBack {
                let patient = PDPatient(forms: selectedForms)
                patient.dateToday = lblDate.text
                let patientInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kFeedBackInfoViewController") as! FeedBackInfoViewController
                patientInfoVC.patient = patient
                self.navigationController?.pushViewController(patientInfoVC, animated: true)
            } else {
                let patient = PDPatient(forms: selectedForms)
                patient.dateToday = lblDate.text
                let patientInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientInfoVC") as! PatientInfoViewController
                patientInfoVC.patient = patient
                patientInfoVC.isNewPatient = self.isNewPatient
                self.navigationController?.pushViewController(patientInfoVC, animated: true)
            }
        } else {
            let alert = Extention.alert("PLEASE SELECT ANY FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    
    
    @IBAction func buttonActionDone(sender: AnyObject) {
        textFieldToothNumbers.resignFirstResponder()
        self.viewToothNumbers.removeFromSuperview()
        self.viewShadow.hidden = true
        let form = formList[2].subForms[textFieldToothNumbers.tag]
        if !textFieldToothNumbers.isEmpty {
            form.isSelected = true
            form.toothNumbers = textFieldToothNumbers.text
        } else {
            form.isSelected = false
        }
        self.tableViewForms.reloadData()
        
    }
    
    
    
    func showAlertPopup() {
        viewAlert.hidden = false
        viewShadow.hidden = false
    }
    
    @IBAction func buttonActionOk(sender: AnyObject) {
        
        viewAlert.hidden = true
        viewShadow.hidden = true
    }

    
    func showPopup() {
        textFieldToothNumbers.text = ""
        self.viewToothNumbers.frame = CGRectMake(0, 0, 512.0, 250.0)
        self.viewToothNumbers.center = self.view.center
        self.viewShadow.addSubview(self.viewToothNumbers)
        self.viewToothNumbers.transform = CGAffineTransformMakeScale(0.1, 0.1)
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewToothNumbers.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension HomeViewControllerAuto : UITableViewDelegate {
    
    func tapGestureDidSelectAction(sender : UITapGestureRecognizer) {
        let indexPath = (sender.view as! FormsTableViewCell).indexPath
        let form = formList[indexPath.section].subForms[indexPath.row]
        form.isSelected = !form.isSelected
        if indexPath.section == 0 {
            if formList[0].subForms[0].isSelected == false {
                for form in formList[0].subForms {
                    form.isSelected = false
                }
            }
            tableViewForms.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.None)
            return
        }

        tableViewForms.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        if form.isToothNumberRequired == true && form.isSelected {
            textFieldToothNumbers.tag = indexPath.row
            self.showPopup()
        }
    }

    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellSubForm") as! FormsTableViewCell
        let form = formList[indexPath.section].subForms[indexPath.row]
        let height = form.formTitle.heightWithConstrainedWidth(392.0, font: cell.labelFormName.font) + 20
        return height
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 102.0
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let form = formList[section]
        let cell = tableView.dequeueReusableCellWithIdentifier("cellHeader") as! FormsTableViewCell
        let headerView = cell.contentView.subviews[1] as! PDTableHeaderView
        headerView.labelFormName.text = form.formTitle
        
        let selectedForm = formList.filter { (obj) -> Bool in
            return obj.isSelected == true
        }
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            if selectedForm.count > 0 {
                UIView.animateWithDuration(0.3) {
                    cell.contentView.alpha = form.isSelected == false ? 0.2 : 1.0
                }
            } else {
                UIView.animateWithDuration(0.3) {
                    cell.contentView.alpha = 1.0
                }
            }
        }
        
        
        headerView.tag = section
        if headerView.gestureRecognizers == nil {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
            headerView.addGestureRecognizer(tapGesture)
        }
        return cell.contentView
    }
    
    func tapGestureAction(sender : UITapGestureRecognizer) {
        
        let headerView = sender.view as! PDTableHeaderView
        let form = self.formList[headerView.tag]
        
        let selectedForm = formList.filter { (obj) -> Bool in
            return obj.isSelected == true
        }
        
        func displayRows() {
            self.view.layoutIfNeeded()
            constraintTableViewTop.constant = 30
            UIView.animateWithDuration(0.3, animations: {
                self.view.layoutIfNeeded()
            }) { (finished) in
                self.constraintTableViewBottom.constant = 30
                self.isNewPatient = form.formTitle == kNewPatient.keys.first ? true : false
                form.isSelected = !form.isSelected
                var indexPaths : [NSIndexPath] = [NSIndexPath]()
                for (idx, _) in form.subForms.enumerate() {
                    let indexPath = NSIndexPath(forRow: idx, inSection: headerView.tag)
                    indexPaths.append(indexPath)
                }
                self.tableViewForms.beginUpdates()
                self.tableViewForms.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Fade)
                self.tableViewForms.endUpdates()
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    self.tableViewForms.scrollToRowAtIndexPath(indexPaths.last!, atScrollPosition: .Bottom, animated: true)
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        self.tableViewForms.userInteractionEnabled = true
                        self.tableViewForms.reloadData()
                    }
                }
            }
        }
        
        self.tableViewForms.userInteractionEnabled = false
        if selectedForm.count > 0 {
            selectedForm[0].isSelected = false
            let section = selectedForm[0].formTitle == kNewPatient.keys.first ? 0 : selectedForm[0].formTitle == kExistingPatient.keys.first ? 1 : 2
            var indexPaths : [NSIndexPath] = [NSIndexPath]()
            
            for (idx, _) in selectedForm[0].subForms.enumerate() {
                let indexPath = NSIndexPath(forRow: idx, inSection: section)
                indexPaths.append(indexPath)
            }
            if form.formTitle == kNewPatient.keys.first {
                for obj in self.formList {
                    for subForm in obj.subForms {
                        subForm.isSelected = false
                    }
                }
            }
            self.tableViewForms.beginUpdates()
            self.tableViewForms.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .None)
            self.tableViewForms.endUpdates()
            
            self.view.layoutIfNeeded()
            constraintTableViewTop.constant = 100
            UIView.animateWithDuration(0.3, animations: {
                self.view.layoutIfNeeded()
            }) { (finished) in
                self.constraintTableViewBottom.constant = 201
                if form.formTitle != selectedForm[0].formTitle {
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.3 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        displayRows()
                    }
                } else {
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        self.tableViewForms.userInteractionEnabled = true
                        self.tableViewForms.reloadData()
                    }
                }
            }
        } else {
            displayRows()
        }
    }
}

extension HomeViewControllerAuto : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return formList.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let form = formList[section]
        return form.isSelected == true ? form.subForms.count : 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellSubForm", forIndexPath: indexPath) as! FormsTableViewCell
        let form = formList[indexPath.section].subForms[indexPath.row]
        cell.labelFormName.text = form.formTitle
        cell.imageViewCheckMark.hidden = !form.isSelected
        cell.labelFormName.textColor = form.isSelected == true ? UIColor(red: 19/255.0, green: 192/255.0, blue: 235/255.0, alpha: 1.0) : UIColor.whiteColor()
        cell.indexPath = indexPath
        if indexPath.section == 0 && indexPath.row > 0 {
            cell.labelFormName.textColor = formList[0].subForms[0].isSelected == true ? (form.isSelected == true ? UIColor(red: 19/255.0, green: 192/255.0, blue: 235/255.0, alpha: 1.0) : UIColor.whiteColor()) : UIColor.lightGrayColor()
        } else {
            cell.labelFormName.textColor = form.isSelected == true ? UIColor(red: 19/255.0, green: 192/255.0, blue: 235/255.0, alpha: 1.0) : UIColor.whiteColor()
        }

        if cell.gestureRecognizers == nil {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureDidSelectAction))
            cell.addGestureRecognizer(tapGesture)
        }
        cell.backgroundColor = UIColor.clearColor()
        cell.contentView.backgroundColor = UIColor.clearColor()
        return cell
    }
}

extension HomeViewControllerAuto : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}



