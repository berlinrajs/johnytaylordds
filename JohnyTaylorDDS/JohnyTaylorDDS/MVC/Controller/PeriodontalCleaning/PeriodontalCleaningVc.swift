//
//  PeriodontalCleaningVc.swift
//  JohnyTaylorDDS
//
//  Created by SRS Web Solutions on 11/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PeriodontalCleaningVc: PDViewController {
    
    @IBOutlet var imageViewPatientSignature: SignatureView!
    @IBOutlet var labelDate: DateLabel!
    
    @IBOutlet var labelPatientNameChange: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelPatientNameChange.text = labelPatientNameChange.text?.stringByReplacingOccurrencesOfString("kPatientName", withString: "\(patient.fullName)")
        
        let string : NSString = labelPatientNameChange.text!
        let range = string.rangeOfString(patient.fullName )
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
           labelPatientNameChange.attributedText = attributedString


        // Do any additional setup after loading the view.
    }

    @IBAction func buttonActionNext(sender: AnyObject) {
        if !imageViewPatientSignature.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("PeriodontalCleaningForm") as! PeriodontalCleaningForm
            patient.patientSignature = imageViewPatientSignature.image
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
    }
    

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
