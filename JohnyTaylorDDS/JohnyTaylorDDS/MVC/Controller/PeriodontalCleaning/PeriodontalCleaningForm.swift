//
//  PeriodontalCleaningForm.swift
//  JohnyTaylorDDS
//
//  Created by SRS Web Solutions on 11/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PeriodontalCleaningForm: PDViewController {
    
    @IBOutlet var imageViewPatientSignature: UIImageView!
    @IBOutlet var labelDate: FormLabel!

    @IBOutlet var labelPatientName: FormLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.text = patient.dateToday
        imageViewPatientSignature.image = patient.patientSignature
        labelPatientName.text = patient.fullName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
