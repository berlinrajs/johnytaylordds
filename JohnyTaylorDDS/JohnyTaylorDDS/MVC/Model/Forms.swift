//
//  Forms.swift
//  WestgateSmiles
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"

let kConsentForms = "CONSENT FORMS"
let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"
let kNewPatientSignInForm = "NEW PATIENT SIGN IN FORM"
let kPatientSignInForm = "PATIENT SIGN IN FORM"


let kToothWhitening = "INFORMED CONSENT FOR PHILIPS ZOOM WHITESPEED TOOTH WHITENING TREATMENT"
let kNitrousOxide = "NITROUS OXIDE INFORMED CONSENT"
let kPeriodontalCleaning = "CONSENT TO PERIODONTAL CLEANING"
let kEndodonticCleaning = "ENDODONTIC CONSENT AND INFORMATION FORM"
let kMedicare = "PRIVATE CONTRACT WITH PATIENT (OPT-OUT OF MEDICARE)"
let kCommunication = "COMMUNICATION RELEASE FORM"
let kRestorationsCrowns = "INFORMED CONSENT FOR RESTORATIONS CROWNS AND BRIDGES"
let KCompositeFillings = "COMPOSITE FILLINGS"
let kToothRemoval = "EXTRACTIONS & WISDOM TEETH REMOVAL WRITTEN INFORMED CONSENT"
let kFinancialPolicy = "FINANCIAL POLICY"
let KHippa = "HIPAA CONSENT"
let kFeedBack = "CUSTOMER REVIEW FORM"

#if AUTO
let kNewPatient = ["NEW PATIENT" : [kNewPatientSignInForm, kInsuranceCard, kDrivingLicense]]
let kExistingPatient = ["EXISTING PATIENT": [kPatientSignInForm, kInsuranceCard, kDrivingLicense]]
let kConsentFormsAuto = ["CONSENT FORMS": [kToothWhitening,kNitrousOxide,kPeriodontalCleaning,kEndodonticCleaning,kMedicare,kCommunication,kRestorationsCrowns,KCompositeFillings,kToothRemoval,kFinancialPolicy,KHippa,kFeedBack]]
#endif

let toothNumberRequired = [kToothRemoval]


class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
    var isSelected : Bool!
    var index : Int!
    var toothNumbers : String!
    var isToothNumberRequired : Bool!

    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (completion :(isConnectionfailed: Bool, forms : [Forms]?) -> Void) {
        let isConnected = Reachability.isConnectedToNetwork()
        #if AUTO
            var arrayResult : [Forms] = [Forms]()
            let forms = [kNewPatient, kExistingPatient, kConsentFormsAuto]
            for form in forms {
                let formObj = getFormObjects(form)
                arrayResult.append(formObj)
            }
            completion(isConnectionfailed: isConnected ? false : true, forms : arrayResult)

        #else
            let forms = [kNewPatientSignInForm,kInsuranceCard,kDrivingLicense, kConsentForms, kFeedBack]
            let formObj = getFormObjects(forms, isSubForm: false)
            completion(isConnectionfailed: isConnected ? false : true, forms : formObj)
        #endif
    }
    
    
    #if AUTO
    private class func getFormObjects (forms : [String : [String]]) -> Forms {
        let formObject = Forms()
        formObject.formTitle = forms.keys.first!
        formObject.isSelected = false
        
        var formList : [Forms] = [Forms]()
        for (_, form) in forms.values.first!.enumerate() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.formTitle = form
            formObj.isToothNumberRequired = toothNumberRequired.contains(form)
            formList.append(formObj)
        }
        formObject.subForms = formList
        return formObject
    }
    #else
    private class func getFormObjects (forms : [String], isSubForm : Bool) -> [Forms] {
        var formList : [Forms]! = [Forms]()
        for (idx, form) in forms.enumerate() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.index = isSubForm ? idx + 4 : idx
            formObj.formTitle = form
            formObj.isToothNumberRequired = toothNumberRequired.contains(form)
            if formObj.formTitle == kConsentForms {
                formObj.subForms = getFormObjects([kToothWhitening,kNitrousOxide,kPeriodontalCleaning,kEndodonticCleaning,kMedicare,kCommunication,kRestorationsCrowns,KCompositeFillings,kToothRemoval,kFinancialPolicy,KHippa], isSubForm:  true)
            }
            if formObj.formTitle == kFeedBack {
                formObj.index = 15
            }
            formList.append(formObj)
        }
        return formList
    }
    #endif
    
}
