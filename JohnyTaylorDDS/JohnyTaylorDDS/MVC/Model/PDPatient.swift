//
//  PDPatient.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
enum InsuranceType: Int {
    case TypeFather = 1, TypeMother, TypeGuardian
}

class PDPatient: NSObject {
    var selectedForms : [Forms]!
    
    #if AUTO
    var patientDetails : PatientDetails?
    #endif

    var gender: Gender!
    var maritialStatus: MaritalStatus!
    
    var firstName : String!
    var lastName : String!
    var middleInitial : String = ""
    var phoneNumber : String!
    var dateToday : String!
    var doctorName: String!
    var preferredName : String!
    var dateOfBirth : String!
    var doctorSign : UIImage = UIImage(named: "DoctorSign")!
    var witnessName : String!
    var relationshipName : String!
    
    //NEW PATIENT
    var parentFirstName : String?
    var parentLastName : String?
    var parentDateOfBirth : String?
    var addressLine : String!
    var cellPhoneNumber: String?
    var patientNumber : String!
    var city : String!
    var zipCode : String!
    var socialSecurityNumber : String?
    var email : String!
    var emergencyContactName : String!
    var emergencyContactRelation: String!
    var emergencyContactPhoneNumber : String!
    var age: String?
    
    var emergencyContactName2: String!
    var emergencyContactRelation2: String!
    var emergencyContactPhoneNumber2 : String!
    
    var lastDentalVisit : String?
    var reasonForTodayVisit : String?
    var isHavingPain : Bool!
    var painLocation : String?
    var anyConcerns : String?
    var hearAboutUs : String?
    var signature1 : UIImage!
    var signature2 : UIImage!
    var inititals1 : UIImage!
    var inititals2 : UIImage!
    var authority : Int!
    
    //Communication
    var communicationSignature: UIImage!
    var communicationWittness: UIImage!
    var communicationEmail: String!
    var communicationCell: String!
    
    //Medicare
    var medicareSignature: UIImage!
    var medicareWitness: UIImage!
    var medicareRelation: String!
    
    var state : String!
    var receieveEmail : Bool!
    var employeeStatus : Int?
    var studentStaus : Int?
    var medicaidID : String?
    var carrierID : String?
    var prefDentist : String?
    var prefPharm : String?
    var preHygen : String?
    var previousDoctor : String?
    var referredBy : String?
    var initial : String?
    var prefferedName : String!
    
    var primaryInsurance : PDInsurance!
    var secondaryInsurance : PDInsurance!
    var checkList: CheckList!
    var patientInformation: PatientInformation = PatientInformation()
    var dentalInformation: DentalInformation!
    var responsibleparty: Person!
    
    var haveInsurance: Int?
    var haveSecondaryInsurance: Int = 2
    var selectedInsurances: [InsuranceType]?

    //PATIENT EMPLOYER DETAILS
    var employerName: String?
    var employerAddress: String?
    var employerCity: String?
    var employerState: String?
    var employerZip: String?
    var employerPhone: String?
    var spouseName: String?
    var spouseEmployerName: String?
    var spouseWorkPhone: String?
    //HipaaConsent
    var hipaaInitial1 : UIImage!
    var hipaaInitial2 : UIImage!
    var hipaaInitial3 : UIImage!
    var hipaaInitial4 : UIImage!
    var hipaaName1 : String!
     var hipaaName2 : String!
     var hipaaName3 : String!
    var hipaaphone1 : String!
    var hipaaPhone2 : String!
    var hipaaPhone3 : String!
    var hipaaRelationShip1 :String!
    var hipaaRelationShip2 :String!
    var hipaaRelationShip3 :String!
    var arrayHipaa : NSArray!
    
    

    //STUDENT DETAILS
    var isStudent: Int?
    var schoolName: String?
    var schoolState: String?
    var schoolCity: String?
    var studyType: Int?
    var referrelSource: String?
    var schoolPhone: String?
    // ToothRemoval
    var textOthers1 : String!
    var textOthers2 : String!
    var textOthers3 : String!
    var toothRemoval : ToothRemoval!
    var initialsImage : UIImage!
    var arrayToothRemoval : NSArray!


    //MEDICAL HISTORY1
    var medicalHistoryQuestions1 : [PDQuestion]!
    var medicalHistoryQuestions2 : [PDQuestion]!
    var medicalHistoryOptions1 : [PDOption]!
    var medicalHistoryOptions2 : [PDOption]!
    var medicalHistoryOptions3 : [PDOption]!
    // Composite fillings
    var arrayCompositeFilling1 : NSArray!
    var arrayCompositeFilling2 : NSArray!
    //Nitrous Oxide
    var patientSignature: UIImage!
    var doctorSignature : UIImage!
    var witnessSignature : UIImage!
    
    var feedBackComment: String!
    var allowMessage: Bool = false
    var isAnonymous: Bool = false
    var rating: CGFloat!
    
    var fullName: String {
        return middleInitial == "" ? firstName + " " + lastName : firstName + " " + middleInitial + " " + lastName
    }
    
    //SCAN CARDS
    var frontImage: UIImage?
    var backImage: UIImage?
    
    // RESTORATIONS,CROWNS,BRIDGES
    //    var arrayRestorationsQuestion1 : NSArray!
    var arrayRestorationsQuestion2 : NSArray!
    var textOthers : String!
    var popUpText : String!
    var button7 : Bool!


    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
}
class ToothRemoval: NSObject {
    
    var removalToothNo: String = ""
    var removalBp1: String = ""
    var removalBp2: String = ""
    var removalPulse: String = ""
    required override init() {
        super.init()
    }
    
}
class PDInsurance : NSObject {
    var insSelfButton : Int!
    var name : String?
    var birthDate: String?
    var relationShipToPatient: String?
    var socialSecurityNumber: String = ""
    var dateEmployed: String = ""
    var nameOfEmployer: String = ""
    var union: String = ""
    var homePhone: String = ""
    var workPhone: String = ""
    var address: String = ""
    var city: String = ""
    var state: String = ""
    var zip: String = ""
    var insuranceCompName: String = ""
    var insurancePlanName: String = ""
    var insuranceCompPhone: String = ""
    var insuranceGroup: String = ""
    var insurancePolicyID: String = ""
    var insuranceCompanyAddress: String = ""
    var insuranceCompanyCity: String = ""
    var insuranceCompanyState: String = ""
    var insuranceCompanyZip: String = ""
    var deductible: String = ""
    var used: String = ""
    var maximumAnualBenifit: String = ""
    var insuranceType: InsuranceType!
    var mailID: String = ""
    var isCoverageAvailable: Int?
}
class Person {
    var personName: String!
    var reloationShipToPatient: String?
    var address: String?
    var state: String?
    var city: String?
    var zip: String?
    var email: String?
    var homePhone: String?
    var cellPhone: String?
    var driverLicence: String?
    var birthDate: String?
//    var financialInstitution: String?
    var employer: String?
    var workPhone: String?
    var socialSecurity: String?
    var resSelfButton : Int!
    var isPatient: Int!
    var paymentMethod: Int!
}

class PatientInformation: NSObject {
    var patientOrGuardianSignature: UIImage!
    
    var physicianName: String?
    var officePhone: String?
    var dateOfLastExam: String?
    
    var contactLense: Int?
    var persistentCough: Int?
    var isPregnent: Int?
    var isNursing: Int?
    var oralContraceptives: Int?
    var isWomen: Int?
    
    var questionGroup1: [PDQuestion]!
    var questionGroup2: [[PDQuestion]]!
    var questionGroup3: [PDQuestion]!
    
    override init() {
        super.init()
        let quest1: [String] = ["Are you under medical treatment now?",
                                "Have you ever been hospitalized for any surgical operation or serious illness within the last 5 years?",
                                "Are you taking any medication(s) including non-prescription medicine?",
                                "Have you ever taken Fen-Phen/Redux?",
                                "Have you ever taken Fosamax, Boniva, Actonel or any cancer medications containing biphosphonates?",
                                "Have you taken Viagra, Revatio, Cialis or Levitra in the last 24 hours?",
                                "Do you use tobacco?",
                                "Do you use controlled substances?"]
        
        let quest21: [String] = ["High Blood Pressure",
                                 "Heart Attack",
                                 "Rheumatic Fever",
                                 "Swollen Ankles",
                                 "Fainting / Seizures",
                                 "Asthma",
                                 "Low Blood Pressure",
                                 "Epilepsy / Convulsions",
                                 "Leukemia",
                                 "Diabetes",
                                 "Kidney Diseases",
                                 "AIDS or HIV Infection",
                                 "Thyroid Problem",
                                 "Heart Disease",
                                 "Cardiac Pacemaker",
                                 "Heart Murmur",
                                 "Angina",
                                 "Frequently Tired",
                                 "Anemia"]
        
        let quest22: [String] = ["Emphysema","Cancer",
                                 "Arthritis",
                                 "Joint Replacement or Implant",
                                 "Hepatitis / Jaundice",
                                 "Sexually Transmitted Disease",
                                 "Stomach Troubles / Ulcers",
                                 "Chest Pains",
                                 "Easily Winded",
                                 "Stroke",
                                 "Hay Fever / Allergies",
                                 "Tuberculosis",
                                 "Radiation Therapy",
                                 "Glaucoma",
                                 "Recent Weight Loss",
                                 "Liver Disease",
                                 "Heart Trouble",
                                 "Respiratory Problems",
                                 "Mitral Valve Prolapse",
                                 "Other"]
        
        let quest3: [String] = ["Local Anesthetics (e.g. Novocain)",
                                "Penicillin or any other Antibiotics",
                                "Sulfa Drugs",
                                "Barbiturates",
                                "Sedatives",
                                "Iodine",
                                "Aspirin",
                                "Any Metals (e.g. nickel, mercury, etc.)",
                                "Latex Rubber",
                                "Other"]
        
        self.questionGroup1 = PDQuestion.arrayOfQuestions(quest1)
        self.questionGroup1[1].isAnswerRequired = true
        self.questionGroup1[2].isAnswerRequired = true
        
        self.questionGroup2 = [PDQuestion.arrayOfQuestions(quest21), PDQuestion.arrayOfQuestions(quest22)]
        self.questionGroup2[1].last?.isAnswerRequired = true
        
        self.questionGroup3 = PDQuestion.arrayOfQuestions(quest3)
        self.questionGroup3.last?.isAnswerRequired = true
    }
}
class DentalInformation : NSObject {
    var previousDentistNameAndLocation: String?
    var dateOfLastExam: String?
    var reasonOfVisit: String?
    
    var clicking: Int?
    var pain: Int?
    var openOrClose: Int?
    var chewing: Int?
    
    var questionGroup1: [PDQuestion]!
    var questionGroup2: [PDQuestion]!
    
    class func adultDentalInformation() -> DentalInformation
    {
        let dentalInfo = DentalInformation()
        
        let quest1: [String] = ["Do your gums bleed while brushing or flossing?",
                                "Are your teeth sensitive to hot or cold liquids/foods?",
                                "Are your teeth sensitive to sweet or sour liquids/foods?",
                                "Do you feel pain to any of your teeth?",
                                "Do you have any sores or lumps in or near your mouth?",
                                "Have you had any head, neck or jaw injuries?"]
        
        dentalInfo.questionGroup1 = PDQuestion.arrayOfQuestions(quest1)
        
        let quest2: [String] = ["Do you have frequent headaches?",
                                "Do you clench or grind your teeth?",
                                "Do you bite your lips or cheeks frequently?",
                                "Have you ever had any difficult extractions in the past?",
                                "Have you ever had any prolonged bleeding following extractions?",
                                "Have you had any orthodontic treatment?",
                                "Do you wear dentures or partials?",
                                "Have you ever received oral hygiene instructions regarding the care of your teeth and gums?",
                                "Do you like your smile?"]
        
        dentalInfo.questionGroup2 = PDQuestion.arrayOfQuestions(quest2)
        dentalInfo.questionGroup2[6].isAnswerRequired = true
        dentalInfo.questionGroup2.last?.isAnswerRequired = true
        
        return dentalInfo
    }
    class func childDentalInformation() -> DentalInformation
    {
        let dentalInfo = DentalInformation()
        
        let quest1: [String] = ["Has child complaint about dental problems?",
                                "Does child brush teeth daily?",
                                "Does child use floss everyday?",
                                "Is fluoride taken in any form?",
                                "Any injuries to mouth, teeth, head?",
                                "Any unhappy dental experiences?",
                                "Any mouth habits - thumbsucking, nail biting, mouth breathing, pacifier, sleeping with bottle, etc?"]
        
        dentalInfo.questionGroup1 = PDQuestion.arrayOfQuestions(quest1)
        
        return dentalInfo
    }
}

class CheckList: NSObject {
    var oftenBrush: String?
    var oftenFloss: String?
    var height: String?
    var weight: String?
    //    var bloodPressure: String?
    var otherHealthCondition: String?
    
    var rapidQuestions: [[PDQuestion]]!
    
    override init() {
        super.init()
        let quest1: [String] = ["Do your gums bleed?",
                                "Are your gums sore or swollen?",
                                "Have your gums receded (do teeth look longer)?",
                                "Are your teeth loose?",
                                "Do you smoke or use tobacco products?",
                                "Do you drink excessively?",
                                "Do you have a persistent sore throat or ear pain?",
                                "Do you have unexplained numbness or pain in the face/neck/mouth?",
                                "Do you have a sore or lesion on the lips or mouth that has persisted for 2 weeks or more?"]
        
        let quest2: [String] = ["Do you have chronic hoarseness?",
                                "Do you have difficulty chewing, swallowing, or moving the jaw or tongue?",
                                "Do you have a lump or thickening in the cheek?",
                                "Do you snore or have you been told in the past you snore?",
                                "Do you regularly have excessive daytime sleepiness?",
                                "Have you been diagnosed with sleep apnea?",
                                "Do you have a heart condition?",
                                "Is there a history of heart disease in your immediate family?",
                                "Do you have a family history of diabetes?",
                                "Do you have high cholesterol?",
                                "Do you have any other health conditions?"]
        
        let questions1 = PDQuestion.arrayOfQuestions(quest1)
        let questions2 = PDQuestion.arrayOfQuestions(quest2)
        questions2.last?.isAnswerRequired = true
        
        self.rapidQuestions = [questions1, questions2]
    }
}

enum MaritalStatus: String {
    case SINGLE = "S"
    case MARRIED = "M"
    case WIDOWED = "W"
    case DIVORCED = "D"
    case SEPARATE = "X"
    case UNKNOWN = "U"
    case DEFAULT = "DE"

    init(value: Int) {
        switch value {
        case 1: self = .SINGLE
        case 2: self = .MARRIED
        case 3: self = .WIDOWED
        case 4: self = .DIVORCED
        case 5: self = .SEPARATE
        case 6: self = .UNKNOWN
        default: self = .DEFAULT
        }
    }
    var index: Int {
        switch self {
        case .SINGLE: return 1
        case .MARRIED: return 2
        case .WIDOWED: return 3
        case .DIVORCED: return 4
        case .SEPARATE: return 5
        case .UNKNOWN: return 6
        default: return 7
        }
    }
    var description: String {
        switch self {
        case .SINGLE: return "SINGLE"
        case .MARRIED: return "MARRIED"
        case .WIDOWED: return "WIDOWED"
        case .DIVORCED: return "DIVORCED"
        case .SEPARATE: return "SEPARATED"
        case .UNKNOWN: return "UNKNOWN"
        default: return "DEFAULT"
        }
    }
}

enum Gender: String {
    case MALE = "M"
    case FEMALE = "F"
    case DEFAULT = "D"

    init(value: Int) {
        switch value {
        case 1: self = .MALE
        case 3: self = .FEMALE
        default: self = .DEFAULT
        }
    }
    var index: Int {
        switch self {
        case .MALE: return 1
        case .FEMALE: return 2
        default: return 3
        }
    }
    
    var description: String {
        switch self {
        case .MALE: return "MALE"
        case .FEMALE: return "FEMALE"
        default: return "DEFAULT"
        }
    }
}

#if AUTO
    class PatientDetails : NSObject {
        var dateOfBirth : String!
        var firstName : String!
        var lastName : String!
        var preferredName : String!
        var address : String!
        var city : String!
        var state : String!
        var zipCode : String!
        var country : String!
        var gender : Gender?
        var socialSecurityNumber : String!
        var email : String!
        var homePhone : String!
        var workPhone : String!
        var cellPhone : String!
        
        
        var patientNumber : String!
        var maritalStatus: MaritalStatus?
        var middleInitial : String!
        
        override init() {
            super.init()
        }
        
        init(details : [String: AnyObject]) {
            super.init()
            self.city = getValue(details["City"])
            self.dateOfBirth = getValue(details["Birthdate"])
            self.email = getValue(details["Email"])
            self.socialSecurityNumber = getValue(details["SSN"])
            self.address = getValue(details["Address"])
            self.zipCode = getValue(details["Zip"])
            self.gender = getValue(details["sex"]) == "" ? nil : Gender(rawValue: getValue(details["sex"]))
            self.firstName = getValue(details["FName"])
            self.patientNumber = getValue(details["PatNum"])
            self.state = getValue(details["State"])
            self.homePhone = getValue(details["HmPhone"]).toPhoneNumber()
            self.workPhone = getValue(details["work_phone"]).toPhoneNumber()
            self.cellPhone = getValue(details["cellular_phone"]).toPhoneNumber()
            self.country = getValue(details["Country"])
            self.lastName = getValue(details["LName"])
            self.preferredName = getValue(details["Preferred"])
            self.maritalStatus = getValue(details["marital_status"]) == "" ? nil : MaritalStatus(rawValue: getValue(details["marital_status"]))
            self.middleInitial = getValue(details["MiddleI"])
        }
        
        func getValue(value: AnyObject?) -> String {
            if value is String {
                return (value as! String).uppercaseString
            }
            return ""
        }
    }
#endif
