//
//  HomePageTableViewCell.swift
//  AdultMedicalForm
//
//  Created by SRS Web Solutions on 13/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HomePageTableViewCell: UITableViewCell {

    @IBOutlet var lblName: UILabel!
    
    @IBOutlet var imgViewCheckMark: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    self.backgroundColor = UIColor.clearColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
