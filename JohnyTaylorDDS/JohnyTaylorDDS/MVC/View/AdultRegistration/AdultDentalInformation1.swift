//
//  AdultDentalInformation1.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AdultDentalInformation1: UIView, UITableViewDataSource, UITableViewDelegate {
    
    var arrayQuestions: [PDQuestion]! {
        didSet {
            (self.viewWithTag(100) as! UITableView!).reloadData()
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return arrayQuestions == nil ? 0 : 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("kAdultDentalInfoCell1", forIndexPath: indexPath) as! AdultDentalInfoCell1
        if(indexPath.row != 6) {
//            if indexPath.row > 6 {
//                
//            } else {
                 cell.configWithQuestion(arrayQuestions[indexPath.row], atIndex: indexPath.row + 1)
//            }
        } else {
            cell.configWithTitle("Have you ever experienced any of the following problems in your jaw?")
        }
        
        return cell
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.row == 6 ? 29.0 : 18
    }
}
class AdultDentalInfoCell1: UITableViewCell {
    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var radioYes: RadioButton!
    
    func configWithQuestion(question: PDQuestion, atIndex: Int) {
        if atIndex > 6 {
            labelCount.text = ""
        } else {
            labelCount.text = "\(atIndex)."
        }
        labelTitle.text = question.question + " ..............................................................................................................."
        radioYes.setSelected(question.selectedOption)
        
        (radioYes.groupButtons[0] as! RadioButton).hidden = false
        (radioYes.groupButtons[1] as! RadioButton).hidden = false
    }
    func configWithTitle(title: String) {
        labelCount.text = "7."
        labelTitle.text = title
        
        (radioYes.groupButtons[0] as! RadioButton).hidden = true
        (radioYes.groupButtons[1] as! RadioButton).hidden = true
    }
}
