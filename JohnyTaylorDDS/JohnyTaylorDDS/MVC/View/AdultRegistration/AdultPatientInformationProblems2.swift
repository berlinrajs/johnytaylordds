//
//  AdultPatientInformationProblem2.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AdultPatientInformationProblems2: UIView {

    var arrayOfQuestions: [PDQuestion]! {
        didSet {
            (self.viewWithTag(100) as! UITableView).reloadData()
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return arrayOfQuestions == nil ? 0 : 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 12 {
            let cell = tableView.dequeueReusableCellWithIdentifier("kAdultProblems2Cell2", forIndexPath: indexPath) as! AdultProblems2Cell
            cell.configWithQuestionAnswers(arrayOfQuestions[indexPath.row])
            
            return cell
        }
        let cell = tableView.dequeueReusableCellWithIdentifier("kAdultProblems2Cell", forIndexPath: indexPath) as! AdultProblems2Cell
        cell.configWithQuestions(arrayOfQuestions[indexPath.row])
        
        return cell
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 13
    }
}

class AdultProblems2Cell: UITableViewCell {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var radioYes: RadioButton!
    
    @IBOutlet weak var labelAnswer1: UILabel?
    @IBOutlet weak var labelAnswer2: UILabel?
    @IBOutlet weak var labelAnswer3: UILabel?
    
    func configWithQuestions(question: PDQuestion) {
        backgroundColor = UIColor.clearColor()
        contentView.backgroundColor = UIColor.clearColor()
        
        labelTitle.text = question.question + " ........................................................................................."
        radioYes.setSelected(question.selectedOption)
    }
    func configWithQuestionAnswers(question: PDQuestion) {
        backgroundColor = UIColor.clearColor()
        contentView.backgroundColor = UIColor.clearColor()
        
        radioYes.setSelected(question.selectedOption)
        if question.selectedOption == true {
            labelAnswer1?.text = question.answer
        }
    }
}