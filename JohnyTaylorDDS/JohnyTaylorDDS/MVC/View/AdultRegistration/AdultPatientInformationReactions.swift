//
//  AdultPatientInformationReactions.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AdultPatientInformationReactions: UIView, UITableViewDataSource, UITableViewDelegate {
    
    var arrayOfQuestions: [PDQuestion]! {
        didSet {
            (self.viewWithTag(100) as! UITableView).reloadData()
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return arrayOfQuestions == nil ? 0 : 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("ReactionsCell", forIndexPath: indexPath) as! AdultReactionsCell
        cell.configWithQuestion(arrayOfQuestions[indexPath.row], atIndex: indexPath.row)
        
        return cell
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfQuestions.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.row == 9 ? 36 : 15
    }
}
class AdultReactionsCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var radioYes: RadioButton!
    
    @IBOutlet weak var labelAnswer1: FormLabel!
    @IBOutlet weak var labelAnswer2: FormLabel!
    
    var question: PDQuestion!
    
    func configWithQuestion(question: PDQuestion, atIndex: Int) {
        
        self.question = question
        
        backgroundColor = UIColor.clearColor()
        contentView.backgroundColor = UIColor.clearColor()
        
        labelTitle.text = question.question + " ........................................................................................."
        radioYes.setSelected(question.selectedOption)
        
        if atIndex == 9 {
            
            (radioYes.groupButtons[0] as! RadioButton).hidden = true
            (radioYes.groupButtons[1] as! RadioButton).hidden = true
            
            labelTitle.text = "Other (please list)"
            
            labelAnswer1.hidden = false
            labelAnswer2.hidden = false
            
//            var rect = labelTitle.frame
//            labelTitle.sizeToFit()
//            rect.size.width = CGRectGetWidth(labelTitle.frame) + 3
//            labelTitle.frame = rect
        }
        else {
            (radioYes.groupButtons[0] as! RadioButton).hidden = false
            (radioYes.groupButtons[1] as! RadioButton).hidden = false
            
            labelAnswer1.hidden = true
            labelAnswer2.hidden = true
//            var rect = labelTitle.frame
//            rect.size.width = 234
//            labelTitle.frame = rect
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        if question != nil {
            if question.isAnswerRequired == true {
//                labelAnswer1.text = question.answer
                question.answer?.setTextForArrayOfLabels([labelAnswer1, labelAnswer2])
            }
        }
    }
}
