//
//  AdultPatientinformation.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AdultPatientInformation: UIView, UITableViewDataSource, UITableViewDelegate {
    
    var arrayOfQuestions: [PDQuestion]! {
        didSet {
            (self.viewWithTag(100) as! UITableView).reloadData()
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return arrayOfQuestions == nil ? 0 : 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CellInfo", forIndexPath: indexPath) as! AdultPatientInfoCell
        
        cell.configWithQuestion(arrayOfQuestions[indexPath.row], atQuestionIndex: indexPath.row + 1)
        return cell
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 4 || indexPath.row == 5 {
//            return 29.0
//        }
        return indexPath.row == 1 ? 65.0 : indexPath.row == 2 ? 83 : indexPath.row == 4 ? 29 : indexPath.row == 5 ? 29.0 : 18.0
    }
//    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return section == 1 ? 36.0 : section == 2 ? 54.0 : 0.0
//    }
//    
//    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        if section == 1 {
//            let view = UIView(frame: CGRectMake(0, 0, CGRectGetWidth(self.frame), 36))
//            
//            return nil
//        } else if section == 2 {
//            return nil
//        } else {
//            return nil
//        }
//    }
}
class AdultPatientInfoCell: UITableViewCell {
    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var radioYes: RadioButton!
    
    @IBOutlet weak var labelSubtitle: UILabel!
    @IBOutlet weak var labelAnswer1: FormLabel!
    @IBOutlet weak var labelAnswer2: FormLabel!
    @IBOutlet weak var labelAnswer3: FormLabel!
    
    var question: PDQuestion!
    
    func configWithQuestion(question: PDQuestion, atQuestionIndex: Int) {
        
        self.question = question
        
        backgroundColor = UIColor.clearColor()
        contentView.backgroundColor = UIColor.clearColor()
        
        labelCount.text = "\(atQuestionIndex)."
        labelTitle.text = question.question + " ........................................................................................."
        radioYes.setSelected(question.selectedOption!)
        
        labelSubtitle.text = atQuestionIndex == 2 ? "If yes, please explain" : atQuestionIndex == 3 ? "If yes, what medication(s) are you taking?" : ""
        
        var rect = labelSubtitle.frame
        labelSubtitle.sizeToFit()
        rect.size.width = CGRectGetWidth(labelSubtitle.frame) + 3
        labelSubtitle.frame = rect
    }
    override func layoutSubviews() {
        var rect = labelSubtitle.frame
        labelSubtitle.sizeToFit()
        rect.size.width = CGRectGetWidth(labelSubtitle.frame) + 3
        labelSubtitle.frame = rect
        
        super.layoutSubviews()
        if question != nil {
            if question.isAnswerRequired == true {
//                labelAnswer1.text = question.answer
                question.answer?.setTextForArrayOfLabels([labelAnswer1, labelAnswer2, labelAnswer3])
            }
        }
    }
}


