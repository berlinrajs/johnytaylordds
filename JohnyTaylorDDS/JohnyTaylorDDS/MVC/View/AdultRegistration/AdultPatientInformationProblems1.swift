//
//  AdultPatientInformationProblems1.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AdultPatientInformationProblems1: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var arrayOfQuestions1: [PDQuestion]! {
        didSet {
            (self.viewWithTag(100) as! UITableView).reloadData()
        }
    }
    
    var arrayOfQuestions2: [PDQuestion]! {
        didSet {
            (self.viewWithTag(200) as! UITableView).reloadData()
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableView.tag == 100 {
            return arrayOfQuestions1 == nil ? 0 : 1
        } else {
            return arrayOfQuestions2 == nil ? 0 : 1
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if tableView.tag == 100 {
            let cell = tableView.dequeueReusableCellWithIdentifier("kAdultProblems1Cell1", forIndexPath: indexPath) as! AdultProblems1Cell
            cell.configWithQuestions(arrayOfQuestions1[indexPath.row])
            
            return cell
        } else
        {
//            if indexPath.row == 7 {
//                let cell = tableView.dequeueReusableCellWithIdentifier("kAdultProblems1Cell2", forIndexPath: indexPath) as! AdultProblems1Cell
//                cell.configQuestionAnswer(arrayOfQuestions2[indexPath.row])
//                
//                return cell
//            }
            let cell = tableView.dequeueReusableCellWithIdentifier("kAdultProblems1Cell2", forIndexPath: indexPath) as! AdultProblems1Cell
            cell.configWithQuestions(arrayOfQuestions2[indexPath.row])
            
            return cell
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.tag == 100 ? arrayOfQuestions1.count : arrayOfQuestions2.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView.tag == 100 {
            return 18
        } else {
            return 18
        }
    }
}

class  AdultProblems1Cell: UITableViewCell {
    @IBOutlet weak var labelTitle1: UILabel!
    @IBOutlet weak var radioYes1: RadioButton!
    @IBOutlet weak var labelAnswer: FormLabel!
    
    var question: PDQuestion!
    
    func configWithQuestions(question: PDQuestion) {
        backgroundColor = UIColor.clearColor()
        contentView.backgroundColor = UIColor.clearColor()
        labelAnswer?.hidden = !question.isAnswerRequired
        
        labelTitle1.text = question.question + " ........................................................................................."
        radioYes1.setSelected(question.selectedOption)
    }
    
    func configQuestionAnswer(question: PDQuestion) {
        backgroundColor = UIColor.clearColor()
        contentView.backgroundColor = UIColor.clearColor()
        
        labelTitle1.text = question.question + " ........................................................................................."
        radioYes1.setSelected(question.selectedOption)
        
        if question.isAnswerRequired == true && question.selectedOption == true {
            labelAnswer.text = question.answer
            labelAnswer.hidden = false
        }
    }
}