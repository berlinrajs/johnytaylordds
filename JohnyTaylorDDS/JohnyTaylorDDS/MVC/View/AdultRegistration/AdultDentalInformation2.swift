//
//  AdultDentalInformation2.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AdultDentalInformation2: UIView, UITableViewDataSource, UITableViewDelegate {
    
    var arrayQuestions: [PDQuestion]! {
        didSet {
            (self.viewWithTag(100) as! UITableView!).reloadData()
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return arrayQuestions == nil ? 0 : 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 6 {
            let cell = tableView.dequeueReusableCellWithIdentifier("kAdultDentalInfoCell23", forIndexPath: indexPath) as! AdultDentalInfoCell2
            cell.configWithQuestion(arrayQuestions[indexPath.row], atIndex: indexPath.row + 8)
            
            return cell
        }
//        else if indexPath.row == 8 {
//            let cell = tableView.dequeueReusableCellWithIdentifier("kAdultDentalInfoCell22", forIndexPath: indexPath) as! AdultDentalInfoCell2
//            cell.configQuestionAnswer(arrayQuestions[indexPath.row])
//            
//            return cell
//        }
        let cell = tableView.dequeueReusableCellWithIdentifier("kAdultDentalInfoCell21", forIndexPath: indexPath) as! AdultDentalInfoCell2
        cell.configWithQuestion(arrayQuestions[indexPath.row], atIndex: indexPath.row + 8)
        
        return cell
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.row == 3 || indexPath.row == 6 ? 29.0 : indexPath.row == 4 ? 36.0 : indexPath.row == 7 ? 54 : 18
    }
}
class AdultDentalInfoCell2: UITableViewCell {
    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var radioYes: RadioButton!
    
    @IBOutlet weak var labelAnswer1: UILabel!
    @IBOutlet weak var labelAnswer2: UILabel!
    @IBOutlet weak var labelAnswer3: UILabel!
    @IBOutlet weak var labelSubtitle: UILabel!
    
    func configWithQuestion(question: PDQuestion, atIndex: Int) {
        labelCount?.text = "\(atIndex)."
        
        labelTitle?.text = question.question + " ...................................................................................................."
        radioYes?.setSelected(question.selectedOption)
        
        if atIndex == 14 {
            labelAnswer1.text = question.selectedOption == false ? "" : question.answer
        }
    }
    func configQuestionAnswer(question: PDQuestion) {
        radioYes?.setSelected(question.selectedOption)
        
        if question.isAnswerRequired == true && question.selectedOption == true {
            
            if labelAnswer2 != nil {
                question.answer?.setTextForArrayOfLabels([labelAnswer1, labelAnswer2, labelAnswer3])
            } else {
                labelAnswer1?.text = question.answer
            }
        }
    }
}