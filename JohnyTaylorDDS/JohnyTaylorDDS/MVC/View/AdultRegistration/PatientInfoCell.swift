//
//  PatientInfoCell.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/17/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

protocol  PatientInfoCellDelegate {
    func radioButtonTappedForCell(cell: PatientInfoCell)
}

class PatientInfoCell: UITableViewCell {

    @IBOutlet var labelQuestion: UILabel!
    @IBOutlet var radioButtonYes: RadioButton!
    
    var question: PDQuestion!
    var delegate: PatientInfoCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configCell(question: PDQuestion) {
        backgroundColor = UIColor.clearColor()
        contentView.backgroundColor = UIColor.clearColor()
        
        self.question = question
        if(self.question.selectedOption == nil) {
            self.radioButtonYes.deselectAllButtons()
        } else {
            self.radioButtonYes.selected = self.question.selectedOption == true
        }
        self.labelQuestion.text = self.question.question
    }
    
    func configCellWithAsterix(question: PDQuestion) {
        backgroundColor = UIColor.clearColor()
        contentView.backgroundColor = UIColor.clearColor()
        
        self.question = question
        if(self.question.selectedOption == nil) {
            self.radioButtonYes.deselectAllButtons()
        } else {
            self.radioButtonYes.selected = self.question.selectedOption == true
        }
        self.labelQuestion.text = self.question.question + " *"
    }
    
    @IBAction func radioButtonPressed(sender: RadioButton) {
        self.question.selectedOption = sender == radioButtonYes
        if question.isAnswerRequired && self.question.selectedOption {
            self.delegate.radioButtonTappedForCell(self)
        }
    }
}
